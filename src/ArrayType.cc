#include "Types.h"
#include "NewNode.h"

llvm::Type* ArrayType::getLLVMType(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) const {
	auto res = module->getTypeByName(name);
	if ( not res ) {
		res = llvm::StructType::create(context, name);

		if (module->getModuleIdentifier() == "-E") {
			auto length_type = builder.getInt32Ty();
			auto element_type = elementType->getType()->getLLVMType(builder, context, module);
			auto pointer_type = llvm::PointerType::getUnqual(element_type);
			vector<llvm::Type *> content = {length_type, pointer_type};
			res->setBody(content);

			// constructor
			std::vector<llvm::Type *> funcArgs = {llvm::PointerType::getUnqual(res), builder.getInt32Ty()};
			llvm::ArrayRef<llvm::Type *> fargsRef(funcArgs);
			llvm::FunctionType *constrType = llvm::FunctionType::get(builder.getVoidTy(), fargsRef, false);

			auto func = llvm::Function::Create(constrType, llvm::Function::ExternalLinkage, name + "." + name, module);

			auto selfp = func->arg_begin();
			auto point = func->arg_begin();
			point->setName("this");
			point++;
			point->setName("count");
			point--;

			llvm::BasicBlock *entry = llvm::BasicBlock::Create(context, "", func);

			auto prev = builder.saveIP();

			builder.SetInsertPoint(entry);

			// init array count
			auto addr = builder.CreateStructGEP(selfp, 0);
			point++;
			builder.CreateStore(point, addr);

			auto malloc = NewNode::getMalloc(context, module);

			llvm::DataLayout *dl = new llvm::DataLayout(module);
			auto allocated = builder.CreateCall(
					malloc,
					builder.CreateIntCast(builder.CreateMul(point, builder.getInt32(dl->getTypeAllocSize(element_type))), builder.getInt64Ty(), false, "size")
			);
			llvm::Value *tmp = builder.CreateBitCast(allocated, pointer_type, "mallocated");

			auto array_addr = builder.CreateStructGEP(selfp, 1);
			builder.CreateStore(tmp, array_addr);

			builder.CreateRetVoid();
			builder.restoreIP(prev);
		}
	}
	return llvm::PointerType::getUnqual(res);
}
