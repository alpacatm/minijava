#include "IntegerNode.h"
#include "llvm/IR/TypeBuilder.h"

llvm::Value* IntegerNode::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	return builder.getInt32(value);
}