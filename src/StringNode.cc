#include "StringNode.h"
#include "NewNode.h"

#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/Constants.h"

llvm::Value* StringNode::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	llvm::Constant *str = llvm::ConstantDataArray::getString(context, value);

	auto malloc = NewNode::getMalloc(context, module);

	llvm::DataLayout* dl = new llvm::DataLayout(module);

	auto allocated = builder.CreateBitCast(
		builder.CreateCall(
			malloc,
			builder.getInt64( dl->getTypeAllocSize(str->getType()) * (value.size() + 1) )
		),
		llvm::PointerType::getUnqual(str->getType())
	);

	builder.CreateStore(str, allocated);
	return builder.CreateBitCast(allocated, getType()->getLLVMType(builder, context, module));
}