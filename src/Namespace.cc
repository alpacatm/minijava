#include "Namespace.h"
#include "VoidType.h"
#include "AutoType.h"

Namespace::Namespace() {
	pushScope();

	addType(shared_ptr<Type>(new VoidType()));
	// addType(shared_ptr<Type>(new AutoType())); TODO add this type after all

	// Sooo bad
	addType(shared_ptr<Type>(new ArrayType(getType("int"))));
	addType(shared_ptr<Type>(new ArrayType(getType("String"))));
}

shared_ptr<VarNode> Namespace::getVar(const string& name) {
	if (auto var = findVar(name)) {
		//std::cout << (int*)&*currentScope() << ": get found " << var.get()->toJson() << "\n";
		return var.get();
	}
	// ищем в предшествующих скоупах переменную до первого unordered скоупа, в него и добавляем заглушку
	auto new_var = shared_ptr<VarNode>(new VarNode(name, JUNK_TYPE));
	currentScope(true)->names[name] = new_var;
	return new_var;
}

shared_ptr<VarNode> Namespace::addVar(const string& name, const shared_ptr<TypeNode> type) {
	auto iter = currentScope()->names.find(name);
	if (iter == currentScope()->names.end()) {
		shared_ptr<VarNode> node(new VarNode(name, type));
		currentScope()->names[name] = node;
	} else {
		//std::cout << "setting var: " << name << " isa " << type->name << "\n";
		if (not currentScope()->names[name]->type or currentScope()->names[name]->type == JUNK_TYPE) {
			currentScope()->names[name]->type = type;
		} else {
			throw std::runtime_error("Attemp to redeclare already defined variable");
		}
	}

	//std::cout << (int*)&*currentScope() << ": add " << currentScope()->names[name]->toJson() << "\n";
	return currentScope()->names[name];
}

shared_ptr<TypeNode> Namespace::getType(const string& name) {
	if (auto type = findType(name)) return type.get();
	// ищем в предшествующих неупорядоченных скоупах тип
	for (auto scope : scopes) {
		if (scope->is_ordered) continue;

		auto subtype = scope->types.find(name);
		if (subtype != scope->types.end()) {
			return subtype->second;
		}
	}

	// не нашли тип, добавляем в ближайший неупорядоченный (текущий, если он неупорядоченный)
	shared_ptr<Scope> unordered_scope = currentScope(true);
	auto new_type = shared_ptr<TypeNode>(new TypeNode(name));
	unordered_scope->types[name] = new_type;

	return unordered_scope->types[name];
}

shared_ptr<TypeNode> Namespace::addType(shared_ptr<Type> type) {
	auto existing_type_opt = findType(type->name);
	if ( existing_type_opt ) {
		auto existing_type = existing_type_opt.get();
		if (existing_type->is_defined)
			throw runtime_error("Attempt to redefine already defined type: " + type->name);

		// refresh existing
		existing_type->setType(type);
		//std::cout << "type " << type->name << " updated" << "\n";

		return existing_type;
	}

	shared_ptr<Scope> unordered_scope = currentScope(true);
	unordered_scope->types[type->name] = shared_ptr<TypeNode>(new TypeNode(type));
	return unordered_scope->types[type->name];
}

void Namespace::pushScope(bool is_ordered) {
	scopes.emplace_front(new Scope(is_ordered));
	//std::cout << "pushed scope: " << (int*)&*currentScope() << "\n";
}

vector<shared_ptr<VarNode>> Namespace::popScope() {
	auto current_scope = currentScope();
	//std::cout << "poped scope : " << (int*)&*current_scope << "\n";

	scopes.pop_front();
	vector<shared_ptr<VarNode>> homeless;

	if (current_scope->is_ordered) return homeless;

	shared_ptr<Scope> unordered_scope = currentScope(true);

	// выкидываем переменные с типом $junk в ближайший неупорядоченный скоуп
	//std::cout << " { ";
	for (auto var_iter : current_scope->names) {
		shared_ptr<VarNode> var = var_iter.second;
		trash.push_back(var);
		//std::cout << var->toJson();
		if (var->type == JUNK_TYPE) {
			//std::cout << " undef";
			unordered_scope->names[var->name] = var;
			homeless.push_back(var);
		}
		//std::cout << ", ";
	}
	//std::cout << " }\n";

	// выкидываем необъявленные типы в этот скоуп
	for (auto type_iter : current_scope->types) {
		shared_ptr<TypeNode> type = type_iter.second;
		trash.push_back(type);
		if (not type->is_defined) {
			unordered_scope->types[type->name] = type;
		}
	}

	return homeless;
}

optional<shared_ptr<VarNode>> Namespace::findVar(const string& name) {
	for (auto scope : scopes) {
		auto node = scope->names.find(name);
		if (node != scope->names.end()) {
			return optional<shared_ptr<VarNode>>(node->second);
		}
	}

	return optional<shared_ptr<VarNode>>();
}

optional<shared_ptr<TypeNode>> Namespace::findType(const string& name) {
	for (auto scope : scopes) {
		auto node = scope->types.find(name);
		if (node != scope->types.end()) {
			return optional<shared_ptr<TypeNode>>(node->second);
		}
	}

	return optional<shared_ptr<TypeNode>>();
}

shared_ptr<Scope> Namespace::currentScope(bool unordered) {
	if (not unordered) return scopes.front();

	for (auto scope : scopes) {
		if (not scope->is_ordered) {
			return scope;
		}
	}

	throw runtime_error("Missing global scope");
}

bool Namespace::importType(const shared_ptr<ObjectType>& type) {
	auto holder = this->getType(type->name);
	if ( not holder->getType() )
		holder->setType(type->asExternal());
	return true;
}

bool Namespace::exportTypes(const shared_ptr<Namespace>& names) {
	auto res = true;
	auto scope = currentScope();
	for ( auto klass : scope->types )
		if ( auto otype = std::dynamic_pointer_cast<ObjectType>(klass.second->getType()) )
			res = names->importType(otype) && res;
	return res;
}

void Namespace::classesAsVariables() {
	auto scope = currentScope();
	for ( auto var : scope->names )
		if ( var.second->type == JUNK_TYPE ) {
			auto type = scope->types.find(var.first);
			if ( type != scope->types.end() ) {
				this->addVar(var.first, type->second);
				scope->names.find(var.first)->second->is_class = true;
			}
		}
}

llvm::Value* Namespace::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	auto scope = currentScope();
	for ( auto klass : scope->types )
		if ( klass.second->getType() ) // TODO: remove this check, we need it now only because of 'main' type
			klass.second->getType()->getLLVMType(builder, context, module);
	return 0;
}

shared_ptr<TypeNode> Namespace::getVoidType() { return this->getType(VoidType::typeName); }
shared_ptr<TypeNode> Namespace::getAutoType() { return this->getType(AutoType::typeName); }
shared_ptr<TypeNode> Namespace::getIntegerType() { return this->getType("int"); }
shared_ptr<TypeNode> Namespace::getBooleanType() { return this->getType("boolean"); }
shared_ptr<TypeNode> Namespace::getUStringType() { return this->getType("ustring"); }
shared_ptr<TypeNode> Namespace::getStringType() { return this->getType("String"); }