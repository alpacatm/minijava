#include <cstdlib>
#include <cstring>  // for strlen()
#include <stdexcept>
#include <boost/program_options.hpp>
#include <string>
#include <sstream>
#include <set>

#include "AstNode.h"
#include "Module.h"

using std::string;

void _run(const string& cmd, const int& ok_code) {
	if ( std::system(cmd.c_str()) != ok_code )
		throw std::runtime_error("Error while running: " + cmd);
}

int main(int argc, char const* argv[]) {

	namespace po = boost::program_options;
	po::options_description desc("Allowed options");
	desc.add_options()
	("help,h", "describe arguments")
	("compile,c", "compile mode")
	("parse,p", "parse mode")
	("analyze,a", "run semantic analysis after parsing")
	//("leaks,l", "compile with leaks, but no need to link")
	("output,o", po::value<string>(), "result fname")
	("input,i", po::value<string>(), "string to work with")
	("include,I", po::value<std::vector<string>>()->multitoken(), "include dir");

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if ( vm.count("help") ) {
		std::cerr << desc << "\n";
		exit(0);
	}

	if ( not vm.count("compile") ^ (vm.count("parse") || vm.count("analyze")) ) {
		std::cerr << "You should use --compile or --parse mode to work with\n";
		exit(0);
	}

	if ( not vm.count("input") ) {
		std::cerr << "You should set input with --input to work with\n";
		exit(0);
	}

	auto in = vm["input"].as<string>();

	auto env_libs = getenv("ALPACALIBS");
	auto include_dirs = string(env_libs ? env_libs : "") + ":./";
	if ( !vm["include"].empty() ) {
		auto dirs = vm["include"].as<vector<string>>();
		for ( auto dir : dirs )
			include_dirs = dir + ":" + include_dirs;
	}
	setenv("ALPACALIBS", include_dirs.c_str(), 1);

	auto module = Module::fromSource(in);
	if ( vm.count("parse") and not vm.count("analyze")) {
		std::cout << module->toJson() << std::endl;
	} else {
		module->check();
		if ( not vm.count("compile") ) {
			std::cout << "queerly, but everything seems to be ok:)" << std::endl;
			std::cout << module->toJson() << std::endl;
		} else {
			auto out = vm.count("output") ? vm["output"].as<string>() : "a";
			if ( module->compile(out) ) {
				std::set<string> usages;
				module->fillUsages(usages);
				std::stringstream usagess;
				for ( auto use : usages )
					usagess << " " << use;
				_run("clang " + out + ".s " + usagess.str() + " -lgc -o " + out + ( vm.count("output") ? "" : ".out"), 0);
			} else {
				std::cerr << "Error while compiling module" << std::endl;
				exit(1);
			}
		}
	}

	return 0;
}
