#include "ExprNode.h"


string ExprNode::wrongArgumentsMessage() {
	auto self_type = self->getType();
	string self_name = self_type ? self_type->name : "unknown";
	if (params.size() == 0)
		return "Operation '" + fname + "' not supported for type: '" + self_name + "'";
	if (params.size() == 1) {
		auto param_type = params[0]->getType();
		string param_type_name = param_type ? param_type->name : "unknown";
		return "Operation '" + fname + "' not supported for types: '" + self_name + "'"
				+ " and '" + param_type_name + "'";
	}
	throw std::logic_error("ExprNode: params size > 2");
}
