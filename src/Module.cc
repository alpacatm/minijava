#include <fstream>


#include "Module.h"
#include "SemanticAnalyzer.h"

#include "Lexer.hh"


#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/SourceMgr.h"

namespace {
	const vector<string> DEFAULT_USAGES = {
		"baseTypes",
		"universal",
		"io",
		"string",
		"bgc"
	};
	
	const string SOURCE_FILE_EXTENSION = ".al";
	const string EXTERNAL_FILE_EXTENSION = ".ale";
	const string COMPILED_FILE_EXTENSION = ".s";
	const string NAMELESS_MODULE = "-E";
	
	string slurp(const string &fileName) {
		std::ifstream ifs(fileName.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
	
		std::ifstream::pos_type fileSize = ifs.tellg();
		ifs.seekg(0, std::ios::beg);
	
		vector<char> bytes(fileSize);
		ifs.read(&bytes[0], fileSize);
	
		return string(&bytes[0], fileSize);
	}

	string compile_external(const string& inputfile, const string& outfile) {
		auto out = outfile.length() ? outfile : inputfile + COMPILED_FILE_EXTENSION;
		auto cmd = "llc " + inputfile + " -o " + out;
		if ( std::system(cmd.c_str()) )
			throw std::runtime_error("Error while running: " + cmd);
	
		return out;
	}
}


shared_ptr<Module> Module::findByName(const string& module_name) {
	auto result = shared_ptr<Module>(new Module(module_name));
	auto file = result->getPath(true, false);
	if ( file.length() == 0 )
		throw std::runtime_error("Can't find " + module_name);
	auto content = slurp(result->getPath(true, false));
	minijava::Lexer lexer(content.c_str(), content.c_str() + content.length());
	minijava::Parser parser(lexer, &*result);
	if ( auto err = parser.parse() )
		throw std::runtime_error("Error while parsing: " + std::to_string(err));

	return result;
}

shared_ptr<Module> Module::fromSource(const string& source) {
	auto result = shared_ptr<Module>(new Module(NAMELESS_MODULE));
	minijava::Lexer lexer(source.c_str(), source.c_str() + source.length());
	minijava::Parser parser(lexer, &*result);
	if ( auto err = parser.parse() )
		throw std::runtime_error("Error while parsing: " + std::to_string(err));

	return result;
}

Module::Module(const string& module_name) : name(module_name), names(new Namespace()) {
	for ( auto mod : DEFAULT_USAGES )
		if ( name != mod ) 
			usages.push_back(shared_ptr<UsageNode>(new UsageNode(mod, names)));
}

bool Module::check() {
	SemanticAnalyzer analyzer(shared_from_this());
	if ( !analyzer.reconstruction() )
		throw std::runtime_error("Couldn't reconstruct tree");

	if ( !analyzer.check() )
		throw std::runtime_error("There is semantic issues in code");

	return true;
}


string Module::toJson() const {
	stringstream adds;
	for ( auto add : classes )
		adds << add->toJson() << ",";
	stringstream uses;
	for ( auto usage : usages )
		uses << usage->toJson() << ",";
	return "{" + (main ? "'main': '" + main->name + "', " : "") + "'classes' : [" +  adds.str() + "], 'usages' : [" +  uses.str() + "]}";
}

shared_ptr<Type> Module::getType() const {
	return shared_ptr<Type>(new VoidType());
}

vector<shared_ptr<AstNode>> Module::getChildrenForTypeCheck() const {
	vector<shared_ptr<AstNode>> children;
	for (auto subclass : classes) {
		auto subchidren = subclass->getChildrenForTypeCheck();
		children.insert(children.end(), subchidren.begin(), subchidren.end());
	}
	return children;
}

bool Module::_reconstruction_action(int pass, std::map<string,string>& variables) {
	if ( pass == FIRST_PASS )
		// TODO Fix it for search class with the same name ( get it from variable )
		for ( auto klass : classes )
			if ( klass->can_be_main() )
				main = klass;

	auto res = true;
	for ( auto use : usages )
		res = use->reconstruction(pass, variables) && res;
	for ( auto klass : classes )
		res = klass->reconstruction(pass, variables) && res;

	if ( pass == THIRD_PASS ) names->classesAsVariables();

	return res;
}

llvm::Value* Module::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	names->codegen(builder, context, module);
	auto res = main ? main->codegen(builder, context, module) : 0;
	for ( auto klass : classes )
		if ( klass != main )
			klass->codegen(builder, context, module);
	return res;
}

bool Module::exportTypes(const shared_ptr<Namespace>& target_names) {
	// before exporting types we need to know their size if they are external
	auto path = getPath(true, true);
	auto res = true;
	if ( path.length() && llvmModule == nullptr ) {
		llvm::SMDiagnostic err;
		auto mod = ParseIRFile(path, err, this->llvmContext);

		if ( not mod ) {
			std::cerr << "Cant parse " <<  path << ":" << std::endl;
			err.print(path.c_str(), llvm::errs(), true);
			res = false;
		} else {
			this->llvmModule = shared_ptr<llvm::Module>(mod);
			for ( auto klass : classes )
				if ( klass->native && !klass->nativeType && !klass->external ) {
					auto type = mod->getTypeByName(klass->name + "..native_typedef");
					if ( type && type->getNumElements() == 1 ) {
						klass->nativeType = type->getElementType(0);
					} else {
						std::cerr << "There is no definition for native type '" << klass->name << "' in '" << name << "'" << std::endl;
						res = false;
					}
				}
		}
	}
	return names->exportTypes(target_names) && res;
}

void Module::fillUsages(std::set<string>& usages) {
	auto path = this->getPath(false, false);
	if ( usages.find(path + COMPILED_FILE_EXTENSION) == usages.end() ) {
		if ( path.length() ) // Don't care about inline modules, there can't be a recursion
			usages.insert(path + COMPILED_FILE_EXTENSION);
		for ( auto use : this->usages )
			use->fillUsages(usages);
	}
}

bool Module::compile(const string& outfile) {
	if ( compiled ) return true;

	auto prefix = outfile;
	if ( not prefix.length() ) {
		prefix = this->getPath(false, false);
		if ( not prefix.length() )
			throw std::runtime_error("Need outfile to compile " + name);
	}

	compiled = true;

	if ( llvmModule == nullptr ) llvmModule = shared_ptr<llvm::Module>(new llvm::Module(name, llvmContext));
	llvm::IRBuilder<> builder(llvmContext);

	if ( auto main_fun = this->codegen(builder, llvmContext, &*llvmModule) ) {

		llvm::FunctionType *funcType =
			llvm::FunctionType::get(builder.getVoidTy(), false);
		llvm::Function *mainFunc =
			llvm::Function::Create(funcType, llvm::Function::ExternalLinkage, "main", &*llvmModule);
	
		llvm::FunctionType *gcFuncType = llvm::FunctionType::get(builder.getVoidTy(), false);
		llvm::Function *init_gc = llvm::Function::Create(gcFuncType, llvm::Function::ExternalLinkage, "bgc_init", &*llvmModule);
	
		llvm::BasicBlock *entry = llvm::BasicBlock::Create(llvmContext, "entrypoint", mainFunc);
		builder.SetInsertPoint(entry);
	
		builder.CreateCall(init_gc);
	
		builder.CreateCall(main_fun);
	
		builder.CreateRetVoid();
	}

	string errstr;
	llvm::raw_fd_ostream rfo((prefix + ".ll").c_str(), errstr,  llvm::sys::fs::OpenFlags::F_None);
	if ( not errstr.empty() )
		throw std::runtime_error(errstr);

	llvmModule->print(rfo, 0);

	compile_external(prefix + ".ll", prefix + COMPILED_FILE_EXTENSION);

	return true;
}


string Module::getPath(const bool& with_extension, const bool& external) const {
	auto extension = external ? EXTERNAL_FILE_EXTENSION : SOURCE_FILE_EXTENSION;

	string res_path = name;
	std::replace( res_path.begin(), res_path.end(), '.', '/');

	auto include_dirs = string(getenv("ALPACALIBS"));
	if ( include_dirs.back() != ':' ) include_dirs = include_dirs + ":";

	size_t pos = 0;
	while ( (pos = include_dirs.find(":")) != string::npos ) {
		string dir = include_dirs.substr(0, pos);
		if ( dir.back() != '/' ) dir = dir + "/";

		if ( access((dir + res_path + extension).c_str(), R_OK) != -1 )
			return dir + res_path + (with_extension ? extension : "");

		include_dirs.erase(0, pos + 1);
	}

	return "";
}