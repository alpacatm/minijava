#include "CallNode.h"
#include "ObjectElement.h"
#include "ObjectType.h"
#include "SemanticAnalyzer.h"


shared_ptr<Type> CallNode::getType() const {
	if (not function)
		throw std::runtime_error("Call wasn't bounded to function: " + toJson());

	return function->getType();
}

bool CallNode::_reconstruction_action(int pass, std::map<string,string>& variables) {
	bool res = true;

	res = self->reconstruction(pass, variables) && res;
	for ( auto param : params )
		res = param->reconstruction(pass, variables) && res;

	if ( pass == FOURTH_PASS && self != nullptr ) {
		res = false;
		auto self_type = self->getType();
		auto class_only = false;
		if ( auto self_as_var = std::dynamic_pointer_cast<VarNode>(self) )
			class_only = self_as_var->is_class;

		if ( self_type ) {
			if ( auto klass = std::dynamic_pointer_cast<ObjectType>(self_type) ) {
				auto fname = this->fname;
				function = klass->findFunction(params,
						[&fname, &class_only] (const shared_ptr<FunNode>& fn) {
						    return fn->getName() == fname && (class_only ? fn->class_function : true);
						}
				);
				if (function) res = true;
			}
		}
		if ( not res )
			std::cerr << wrongArgumentsMessage() << std::endl;
	}

	if (function != nullptr) function->reconstruction(pass, variables);

	return res;
}

llvm::Value* CallNode::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	vector<llvm::Value*> cparams = {};
	llvm::Value* funcp;
	if ( function->class_function ) {
		funcp = function->getLLVMSignature(builder, context, module);
	} else {
		auto self_o = self->codegen(builder, context, module);
		if ( ((ObjectType*) this->function->owner_class)->native ) {
			funcp = function->getLLVMSignature(builder, context, module);
			cparams.push_back(self_o);
		} else {
			auto selfp = builder.CreateBitCast(
				self_o,
				this->function->owner_class->getLLVMType(builder, context, module)
			);
			// pass real method
			funcp = ObjectElement::getLLVMMethod(function, selfp, builder, context, module);
		
			cparams.push_back(selfp);
		}
	}

	for ( size_t pos = 0; pos < params.size(); pos++ ) {
		cparams.push_back(builder.CreateBitCast(
			params[pos]->codegen(builder, context, module),
			this->function->args[pos]->var->getType()->getLLVMType(builder, context, module)
		));
	}
	return builder.CreateCall(funcp, cparams);
}
