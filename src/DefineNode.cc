#include "DefineNode.h"


bool DefineNode::_reconstruction_action(int pass, std::map<string,string>& variables) {
	return var->reconstruction(pass, variables);
}

llvm::Value* DefineNode::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	auto type = var->getType()->getLLVMType(builder, context, module);
	llvm::Value* tmp = builder.CreateAlloca(type, 0, var->name);
	var->setAddr(tmp);
	// maybe default constructor? 0 for int, false for bool, null for array/object
	return tmp;
}