#include "FunNode.h"
#include "ObjectType.h"
#include "BlockNode.h"
#include "ReturnNode.h"
#include "SemanticAnalyzer.h"
#include "llvm/Analysis/Verifier.h"

FunNode::FunNode(const string& fname,
		const bool& is_entrypoint,
		const bool& is_constructor,
		const bool& is_class_function,
		const bool& is_native) :
	rtype(),
	body(),
	args(),
	entrypoint(is_entrypoint),
	constructor(is_constructor),
	class_function(is_class_function),
	native(is_native),
	name(fname),
	signature("") {
}

FunNode::FunNode(const FunNode &other) :
		rtype(other.rtype),
		body(other.body),
		args(other.args),
		entrypoint(other.entrypoint),
		constructor(other.constructor),
		class_function(other.class_function),
		native(other.native),
		name(other.name),
		signature(other.signature) {
}

bool FunNode::_reconstruction_action(int pass, std::map<string,string>& variables) {
	auto res = true;
	switch (pass) {
		case FIRST_PASS:
			if ( body != nullptr
					&& ( !body->body.size() || !std::dynamic_pointer_cast<ReturnNode>(body->body.back()) )
					&& this->isProcedure()
				)
			body = shared_ptr<BlockNode>(new BlockNode(
					{ body, shared_ptr<ReturnNode>(new ReturnNode(optional<shared_ptr<AstNode>>())) } ) );
			break;
		case FIFTH_PASS:
			this->getName();
			this->getRawSignature();
			break;
		default:
			break;
	}

	res = body->reconstruction(pass, variables) && res;
	for ( auto arg : args )
		res = arg->reconstruction(pass, variables) && res;
	res = rtype->reconstruction(pass, variables) && res;
	return res;
}

llvm::Value* FunNode::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
//	llvm::FunctionType *funcType = llvm::FunctionType::get(builder.getVoidTy(), fargsRef, false);
	auto func = this->getLLVMSignature(builder, context, module);

	if ( native ) return func;
	if ( ((ObjectType*) owner_class)->external ) return func;
	if ( not func->isDeclaration() ) return func;

	llvm::BasicBlock *entry = llvm::BasicBlock::Create(context, "", func);

	auto prev = builder.saveIP();

	builder.SetInsertPoint(entry);

	if ( not entrypoint ) {
		auto argnames = func->arg_begin();
		if ( not class_function ) argnames++; // skip 'this' variable
		for ( auto arg : args ) {
			// define local mutable
			auto var = arg->var;
			auto type = var->getType()->getLLVMType(builder, context, module);
			llvm::Value* addr = builder.CreateAlloca(type, 0, var->name);
			var->setAddr(addr);
			// init local mutable
			builder.CreateStore(argnames, addr);
			argnames++;
		}

		if ( not class_function ) {
			auto _this = func->arg_begin();
			auto obj = (ObjectType*) owner_class;
			int index = 0;
			for ( auto var : obj->variables ) {
				index++;
				var->setAddr(builder.CreateBitCast(
					builder.CreateStructGEP(_this, index),
					llvm::PointerType::getUnqual(var->getType()->getLLVMType(builder, context, module)),
					var->name
				), true);
			}

			auto type = obj->self->getType()->getLLVMType(builder, context, module);
			llvm::Value* addr = builder.CreateAlloca(type, 0, "$this");
			obj->self->setAddr(addr);
			builder.CreateStore(_this, addr);
	
			if ( constructor ) {
				// init for vtable
				int index = 0;
				auto vtable_init = module->getNamedGlobal(obj->name + "..vtable..data");
				auto a = builder.CreateStructGEP(_this, index);
				builder.CreateStore(vtable_init, a);
		
				// init for variables
				for ( auto var : obj->variables ) {
					index++;
					// auto addr = builder.CreateStructGEP(_this, index);
					// maybe default constructor? 0 for int, false for bool, null for array/object
				}
			}
		}
	}

	auto block = builder.GetInsertBlock()->getParent();

	llvm::Value* resultVar;
	if ( not this->isProcedure() )
		resultVar = builder.CreateAlloca(this->rtype->getType()->getLLVMType(builder, context, module), 0, "@result");
	auto mainBlock = llvm::BasicBlock::Create(context, "@main");
	builder.CreateBr(mainBlock);

	auto returnFrom = llvm::BasicBlock::Create(context, "@return", block);
	builder.SetInsertPoint(returnFrom);
	if ( entrypoint ) {
		builder.CreateRetVoid();
	} else {
		if ( this->isProcedure() )
			builder.CreateRetVoid();
		else
			builder.CreateRet(builder.CreateLoad(resultVar));
	}

	block->getBasicBlockList().push_back(mainBlock);
	builder.SetInsertPoint(mainBlock);
	body->codegen(builder, context, module);
	builder.CreateBr(returnFrom);

	builder.restoreIP(prev);
	//verifyFunction(*func);

	return func;
}

llvm::Function* FunNode::getLLVMSignature(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) const {
	auto real_name = owner_class->name + "." + signature;
	auto res = module->getFunction(real_name);
	if ( not res ) {
		llvm::FunctionType *funcType;
		if ( not entrypoint ) {
			std::vector<llvm::Type *> funcArgs = {};
			if ( not class_function )
				funcArgs.push_back(owner_class->getLLVMType(builder, context, module));
			for ( auto arg : args ) {
				// TODO: is it even legal?
				funcArgs.push_back(arg->var->type->getType()->getLLVMType(builder, context, module));
			}
			llvm::ArrayRef<llvm::Type*>  fargsRef(funcArgs);
			auto rest = this->rtype->getType()->getLLVMType(builder, context, module);
			funcType = llvm::FunctionType::get(rest, fargsRef, false);
		} else
			funcType = llvm::FunctionType::get(builder.getVoidTy(), false);

		res = llvm::Function::Create(funcType, llvm::Function::ExternalLinkage, real_name, module);

		if ( not (entrypoint || class_function) ) {
			auto argnames = res->arg_begin();
			argnames->setName("this");
		}
	}

	return res;
}

string FunNode::getName() {
	if ( name == "") {
		if (!constructor)
			throw std::logic_error("Non-constructor functions can't have empty names");
		name = owner_class->name;
	}
	return name;
}

string FunNode::getRawSignature() {
	if (signature == "") {
		std::stringstream params_s;
		for (auto arg = args.begin(); arg != args.end(); arg++)
			params_s << (*arg)->var->type->name << (arg == args.end() - 1 ? "" : ",");
		signature = name + "(" + params_s.str() + ")";
	}
	return signature;
}

bool FunNode::canBeCalled(const vector<shared_ptr<AstNode>>& params) {
	vector<shared_ptr<Type>> arg_types;
	for (auto param_iter : params) {
		arg_types.push_back(param_iter->getType());
	}
	return canBeCalled(arg_types);
}

bool FunNode::canBeCalled(const vector<shared_ptr<Type>>& arg_types) {
	if ( arg_types.size() != args.size() ) return false;

	auto arg_iter = arg_types.begin();
	for (auto fun_arg : args) {
		if (arg_iter == arg_types.end()) return false;
		if (**arg_iter != *fun_arg->getType()) return false;
		arg_iter++;
	}
	return true;
}

shared_ptr<Type> FunNode::getType() const {
	return rtype->getType();
}
