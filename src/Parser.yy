%skeleton "lalr1.cc"
%require "2.5"
%defines
%define api.namespace {minijava}
%define parser_class_name {Parser}

%code requires {
	#include <cmath>
	#include <iostream>
	#include <memory>
	#include <vector>
	#include <boost/optional.hpp>
	#include <stdexcept>

	#include "IntegerNode.h"
	#include "BooleanNode.h"
	#include "StringNode.h"
	#include "ArrayElement.h"
	#include "IfNode.h"
	#include "AssignNode.h"
	#include "VarNode.h"
	#include "DefineNode.h"
	#include "FunNode.h"
	#include "CallNode.h"
	#include "ExprNode.h"
	#include "ReturnNode.h"
	#include "ObjectType.h"
	#include "BlockNode.h"
	#include "WhileNode.h"
	#include "NewNode.h"
	#include "UsageNode.h"
	#include "ObjectElement.h"
	#include "Module.h"
	#include "VoidType.h"
	#include "AutoType.h"
	namespace minijava {class Lexer;}


	using std::shared_ptr;
	using std::vector;
	using std::list;

	struct class_inner_content {
		list<shared_ptr<VarNode>> vars;
		list<shared_ptr<FunNode>> methods;
	};
}

%code {

	#include "Lexer.hh"

	#define yylex lexer.lex

	AstNode* _makeOpExpr(const char* op, AstNode* a, AstNode* b) {
		return new ExprNode(shared_ptr<AstNode>(a), op, {shared_ptr<AstNode>(b)});
	}

	AstNode* _makeOpExpr(const char* op, AstNode* a) {
		return new ExprNode(shared_ptr<AstNode>(a), op, {});
	}
}

%parse-param { minijava::Lexer& lexer }
%parse-param { Module* module };

%union {
	IntegerNode *intn;
	StringNode *stringn;
	AstNode *node;
	VarNode *varnode;
	DefineNode *definenode;
	FunNode *funnode;
	Module* modulet;
	TypeNode* type;
	ArrayElement* array_getter;
	ObjectElement* object_getter;
	BlockNode* blockt;

	vector<shared_ptr<AstNode>>* seq;
	vector<shared_ptr<DefineNode>>* defineseq;
	class_inner_content* separateddefines;

	const string *str;
	char symbol;
	bool boolean;
	int integer;
	double doubleprec;
}

%type <modulet> module
%type <integer> INTEGER_VALUE
%type <str> STRING_VALUE
%type <boolean> BOOLEAN_VALUE
%type <node> exp
%type <node> statement
%type <str> IDENTIFIER
%type <varnode> THIS
%type <definenode> var_decl
%type <blockt> block
%type <node> calc
%type <seq> sequence
%type <symbol> lcurve
%type <symbol> rcurve
%type <funnode> fun
%type <funnode> fun_head
%type <str> fun_rtype
%type <funnode> main_fun
%type <funnode> main_fun_head
%type <seq> comma_separated_exps
%type <defineseq> comma_separated_defines
%type <str> class_def_head
%type <separateddefines> class_content
%type <str> type
%type <array_getter> get_array_item
%type <object_getter> get_object_item
%type <str> dot_separated_ident


%token END       0 "end of file"
%token LCURVE   30 "{"
%token RCURVE   31 "}"
%token LPAREN   40 "("
%token RPAREN   41 ")"
%token MUL      42 "*"
%token PLUS     43 "+"
%token MINUS    45 "-"
%token DIV      47 "/"
%token MOD      48 "%"
%token POW      49 "**"
%token AND      50 "&&"
%token OR       51 "||"
%token NOT      52 "!"
%token LE       53 "<"
%token LEQ      54 "<="
%token EQ       55 "=="
%token NE       56 "!="
%token GEQ      57 ">="
%token GE       58 ">"
%token ASSIGN   59 "="
%token LSQUARE  61 "["
%token RSQUARE  62 "]"
%token LRSQUARE 63 "[]"
%token THIS     72 "this"
%token IF      100 "if"
%token THEN    101 "then"
%token ELSIF   102 "elsif"
%token ELSE    103 "else"
%token WHILE   104 "while"
%token RETURN  105 "return"
%token NEW     106 "new"
%token DELIM   110 ";"
%token COMMA   111 ","
%token DOT     112 "."
%token COLON   113 ":"
%token PUBLIC  120 "public"
%token CLASS   130 "class"
%token EXTENDS 131 "extends"
%token FUN     133 "fun"
%token NATIVE  136 "native"
%token USE     150 "use"

%token INTEGER_VALUE
%token STRING_VALUE
%token BOOLEAN_VALUE
%token IDENTIFIER
%token TYPE

%nonassoc CALC

%nonassoc IDENT
%nonassoc GETTER
%nonassoc TYPE

%nonassoc THEN
%nonassoc ELSE
%nonassoc WHILE


%left  OR
%left  AND
%nonassoc LE LEQ EQ NE GEQ GE
%left  PLUS MINUS
%left  MUL DIV MOD
%left  NEG NOT
%right POW
%left LSQUARE
%left DOT

%%

input
	: module { }
	;

module
	: { $$ = module; }
	| module class_def_head LCURVE class_content RCURVE {
		$$ = $1;
		auto super = *$2 == "Universal" ? optional<shared_ptr<TypeNode>>() : optional<shared_ptr<TypeNode>>(module->names->getType("Universal"));
		auto raw_otype = new ObjectType(*$2, vector<shared_ptr<VarNode>>($4->vars.begin(), $4->vars.end()), vector<shared_ptr<FunNode>>($4->methods.begin(), $4->methods.end()), super);
		raw_otype->self = module->names->getVar("$this");
		shared_ptr<Type> objectType = shared_ptr<Type>(raw_otype);
		auto homeless = module->names->popScope();
		raw_otype->variables.insert(raw_otype->variables.end(), homeless.begin(), homeless.end());
		module->names->addType(objectType);
		$1->addClass(std::dynamic_pointer_cast<ObjectType>(objectType));
	  }
	  
	| module NATIVE class_def_head LCURVE class_content RCURVE {
		$$ = $1;
		auto raw_otype = new ObjectType(*$3, vector<shared_ptr<VarNode>>($5->vars.begin(), $5->vars.end()), vector<shared_ptr<FunNode>>($5->methods.begin(), $5->methods.end()));
		raw_otype->native = true;
		raw_otype->self = module->names->getVar("$this");
		shared_ptr<Type> objectType = shared_ptr<Type>(raw_otype);
		auto homeless = module->names->popScope();
		raw_otype->variables.insert(raw_otype->variables.end(), homeless.begin(), homeless.end());
		module->names->addType(objectType);
		$1->addClass(std::dynamic_pointer_cast<ObjectType>(objectType));
	  }
	| module class_def_head EXTENDS type LCURVE class_content RCURVE {
		$$ = $1;
		auto super = module->names->getType(*$4);
		ObjectType* objectType = new ObjectType(*$2, vector<shared_ptr<VarNode>>($6->vars.begin(), $6->vars.end()), vector<shared_ptr<FunNode>>($6->methods.begin(), $6->methods.end()), optional<shared_ptr<TypeNode>>(super));
		objectType->self = module->names->getVar("$this");
		shared_ptr<Type> objectType_ptr = shared_ptr<Type>(objectType);
		auto homeless = module->names->popScope();
		objectType->variables.insert(objectType->variables.end(), homeless.begin(), homeless.end());
		module->names->addType(objectType_ptr);
		$1->addClass(std::dynamic_pointer_cast<ObjectType>(objectType_ptr));
	}
	| module class_def_head LCURVE main_fun RCURVE {
		$$ = $1;
		$1->addClass(shared_ptr<ObjectType>(new ObjectType(*$2, {}, {shared_ptr<FunNode>($4)})));
		module->names->popScope();
	  }
	| module USE dot_separated_ident DELIM {
		$$ = $1;
		$1->addUsage(shared_ptr<UsageNode>(new UsageNode(*$3, module->names)));
	  }
	;

exp
	: calc %prec CALC { $$ = $1; }

	| BOOLEAN_VALUE {
		$$ = new BooleanNode($1, module->names->getBooleanType());
	}
	
	| STRING_VALUE {
		$$ = new NewNode(
			module->names->getStringType(),
			{shared_ptr<AstNode>( new StringNode(*$1, module->names->getUStringType()) )}
		);
	}

	| IDENTIFIER { $$ = module->names->getVar(string(*$1)).get(); }

	| THIS { $$ = module->names->getVar("$this").get(); }

	| exp DOT IDENTIFIER LPAREN comma_separated_exps RPAREN
		{ $$ = new CallNode(shared_ptr<AstNode>($1), *$3, *$5); }

	| NEW IDENTIFIER LSQUARE exp RSQUARE
		{ $$ = new NewNode(module->names->getType("int[]"), {shared_ptr<AstNode>($4)}); /* TODO add support of all arrays */}

	| IDENTIFIER LPAREN comma_separated_exps RPAREN
		{ $$ = new NewNode(module->names->getType(*$1), *$3); }

	| get_array_item %prec GETTER { $$ = $1; }

	| get_object_item %prec GETTER { $$ = $1; }

	| IDENTIFIER ASSIGN exp %prec IDENT
		{ $$ = new AssignNode(module->names->getVar(*$1), shared_ptr<AstNode>($3)); }

	| get_array_item ASSIGN exp %prec IDENT
		{ $$ = new AssignNode(shared_ptr<Addressable>($1), shared_ptr<AstNode>($3)); }

	| get_object_item ASSIGN exp %prec IDENT
		{ $$ = new AssignNode(shared_ptr<Addressable>($1), shared_ptr<AstNode>($3)); }
	;

get_array_item
	: exp LSQUARE exp RSQUARE
		{ $$ = new ArrayElement(shared_ptr<AstNode>($3), shared_ptr<AstNode>($1)); }
	;

get_object_item
	: exp DOT IDENTIFIER
		{ $$ = new ObjectElement(*$3, shared_ptr<AstNode>($1)); }
	;

calc
	: INTEGER_VALUE { $$ = new IntegerNode($1, module->names->getIntegerType()); }
	| exp PLUS exp { $$ = _makeOpExpr("+", $1, $3); }
	| exp MINUS exp { $$ = _makeOpExpr("-", $1, $3); }
	| exp MUL exp { $$ = _makeOpExpr("*", $1, $3); }
	| exp DIV exp { $$ = _makeOpExpr("/", $1, $3); }
	| MINUS exp %prec NEG { $$ = _makeOpExpr("-", $2); }
	| PLUS exp %prec NEG { $$ = _makeOpExpr("+", $2); }
	| exp POW exp { $$ = _makeOpExpr("**", $1, $3); }
	| exp MOD exp { $$ = _makeOpExpr("%", $1, $3); }
	| exp AND exp { $$ = _makeOpExpr("&&", $1, $3); }
	| exp OR exp { $$ = _makeOpExpr("||", $1, $3); }
	| NOT exp %prec NOT { $$ = _makeOpExpr("!", $2); }
	| exp LE exp { $$ = _makeOpExpr("<", $1, $3); }
	| exp LEQ exp { $$ = _makeOpExpr("<=", $1, $3); }
	| exp EQ exp { $$ = _makeOpExpr("==", $1, $3); }
	| exp NE exp { $$ = _makeOpExpr("!=", $1, $3); }
	| exp GEQ exp { $$ = _makeOpExpr(">=", $1, $3); }
	| exp GE exp { $$ = _makeOpExpr(">", $1, $3); }
	| LPAREN exp RPAREN { $$ = $2; }
	;

lcurve
	: LCURVE { $$ = '{'; module->names->pushScope(true); }
	;

rcurve
	: RCURVE { $$ = '}'; module->names->popScope(); }
	;

statement
	: exp DELIM { $$ = $1; }

	| var_decl DELIM { $$ = $1; }

	| var_decl ASSIGN exp DELIM
		{ $$ = new AssignNode(shared_ptr<Addressable>($1), shared_ptr<AstNode>($3)); }

	| IF LPAREN exp RPAREN statement %prec THEN
		{
			$$ = new IfNode(
				shared_ptr<AstNode>($3),
				shared_ptr<AstNode>($5),
				boost::optional<shared_ptr<AstNode>>()
			);
		}

	| IF LPAREN exp RPAREN statement ELSE statement
		{
			$$ = new IfNode(
				shared_ptr<AstNode>($3),
				shared_ptr<AstNode>($5),
				boost::optional<shared_ptr<AstNode>>($7)
			);
		}

	| WHILE LPAREN exp RPAREN statement %prec WHILE
		{ $$ = new WhileNode( shared_ptr<AstNode>($3), shared_ptr<AstNode>($5) ); }

	| block { $$ = $1; }

	| block DELIM { $$ = $1; }

	| RETURN exp DELIM { $$ = new ReturnNode(optional<shared_ptr<AstNode>>($2)); }

	| RETURN DELIM { $$ = new ReturnNode(optional<shared_ptr<AstNode>>()); }
	;

sequence
	: statement { $$ = new vector<shared_ptr<AstNode>>({shared_ptr<AstNode>($1)}); }
	| sequence statement { $$ = $1; $$->push_back(shared_ptr<AstNode>($2)); }
	;

block
	: lcurve rcurve { $$ = new BlockNode(); }
	| lcurve sequence rcurve { $$ = new BlockNode(vector<shared_ptr<AstNode>>(*$2)); }
	;

var_decl
	: type IDENTIFIER { $$ = new DefineNode(module->names->addVar(string(*$2), module->names->getType(*$1))); }
	;

type
	: IDENTIFIER LRSQUARE { $$ = new string(*$1 + "[]"); }
	| IDENTIFIER { $$ = $1; }
	;

fun
	: fun_head LPAREN comma_separated_defines RPAREN fun_rtype block {
		$$ = $1;
		$$->args = *$3;
		$$->body = shared_ptr<BlockNode>($6);
		module->names->popScope();
		$$->rtype = module->names->getType(*$5);
	}
	| fun_head LPAREN comma_separated_defines RPAREN fun_rtype NATIVE {
		$$ = $1;
		$$->args = *$3;
		$$->native = true;
		$$->body = shared_ptr<BlockNode>(new BlockNode());
		module->names->popScope();
		$$->rtype = module->names->getType(*$5);
	}
	;

fun_rtype
	: COLON type { $$ = $2; }
	| COLON { $$ = &AutoType::typeName; }
	| { $$ = &VoidType::typeName; }
	;

fun_head
	: PUBLIC FUN IDENTIFIER { $$ = new FunNode(*$3); module->names->pushScope(true); }
	| PUBLIC FUN THIS { $$ = new FunNode("", false, true); module->names->pushScope(true); }
	| PUBLIC CLASS FUN IDENTIFIER { $$ = new FunNode(*$4, false, false, true); module->names->pushScope(true); }
/* @todo: there should be better solution */
	| PUBLIC FUN MUL { $$ = new FunNode("*"); module->names->pushScope(true); }
	| PUBLIC FUN PLUS { $$ = new FunNode("+"); module->names->pushScope(true); }
	| PUBLIC FUN MINUS { $$ = new FunNode("-"); module->names->pushScope(true); }
	| PUBLIC FUN DIV { $$ = new FunNode("/"); module->names->pushScope(true); }
	| PUBLIC FUN MOD { $$ = new FunNode("%"); module->names->pushScope(true); }
	| PUBLIC FUN POW { $$ = new FunNode("**"); module->names->pushScope(true); }
/* @todo: we should use 'or' and 'and' and 'not' instead of those strange symbols, don't u think so? */
	| PUBLIC FUN AND { $$ = new FunNode("&&"); module->names->pushScope(true); }
	| PUBLIC FUN OR { $$ = new FunNode("||"); module->names->pushScope(true); }
	| PUBLIC FUN NOT { $$ = new FunNode("!"); module->names->pushScope(true); }
	| PUBLIC FUN LE { $$ = new FunNode("<"); module->names->pushScope(true); }
	| PUBLIC FUN LEQ { $$ = new FunNode("<="); module->names->pushScope(true); }
	| PUBLIC FUN EQ { $$ = new FunNode("=="); module->names->pushScope(true); }
	| PUBLIC FUN NE { $$ = new FunNode("!="); module->names->pushScope(true); }
	| PUBLIC FUN GEQ { $$ = new FunNode(">="); module->names->pushScope(true); }
	| PUBLIC FUN GE { $$ = new FunNode(">"); module->names->pushScope(true); }
	;

main_fun
	: main_fun_head LPAREN type IDENTIFIER RPAREN block {
		$$ = $1;
		if ( $$->getName().compare("main") )
			throw std::runtime_error("There can be only 'main' static method, but got '" + $$->getName() + "'");

		$$->body = shared_ptr<BlockNode>($6);
		$$->args = {shared_ptr<DefineNode>(new DefineNode(module->names->addVar(string(*$4), module->names->getType("String[]"))))};
		module->names->popScope();
	}
	;


main_fun_head
	: PUBLIC CLASS IDENTIFIER {
		$$ = new FunNode(*$3, true);
		$$->rtype = module->names->getVoidType();
		module->names->pushScope(true);
	  }
	;

comma_separated_exps
	: { $$ = new vector<shared_ptr<AstNode>>(); }
	| comma_separated_exps COMMA exp { $$ = $1; $1->push_back(shared_ptr<AstNode>($3)); }
	| exp { $$ = new vector<shared_ptr<AstNode>>({shared_ptr<AstNode>($1)}); }
	;

comma_separated_defines
	: { $$ = new vector<shared_ptr<DefineNode>>(); }
	| comma_separated_defines COMMA var_decl { $$ = $1; $1->push_back(shared_ptr<DefineNode>($3)); }
	| var_decl { $$ = new vector<shared_ptr<DefineNode>>({shared_ptr<DefineNode>($1)}); }
	;


class_def_head
	: CLASS IDENTIFIER {
		$$ = $2;
		module->names->pushScope();
		module->names->addVar("$this", module->names->getType(*$2));
	}
	;

class_content
	: { $$ = new class_inner_content(); }
	| var_decl DELIM class_content { $$ = $3; $$->vars.push_front($1->var); }
	| fun class_content { $$ = $2; $$->methods.push_front(shared_ptr<FunNode>($1)); }
	;

dot_separated_ident
	: dot_separated_ident DOT IDENTIFIER { $$ = new string(*$1 + "." + *$3); }
	| IDENTIFIER { $$ = new string(*$1); }
	;
%%

void minijava::Parser::error(const std::string& msg)
{
       std::cerr << msg << '\n';
}

