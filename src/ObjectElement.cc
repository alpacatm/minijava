#include "ObjectElement.h"
#include "ObjectType.h"
#include "SemanticAnalyzer.h"


bool ObjectElement::_reconstruction_action(int pass, std::map<string,string>& variables) {
	auto res = true;
	if ( pass == FOURTH_PASS ) {
		res = false;
		auto self_type = self->getType();
		if ( self_type ) {
			if ( auto obj_type = std::dynamic_pointer_cast<ObjectType>(self_type) ) {
				auto foundo = std::find_if(obj_type->variables.begin(), obj_type->variables.end(), [this] (const shared_ptr<VarNode>& s) { return s->name == name; } );
				if ( foundo != obj_type->variables.end() ) {
					this->variable = *foundo;
					res = true;
				}
			}
			if ( auto obj_type = std::dynamic_pointer_cast<ArrayType>(self_type) ) {
				if ( name == "length" ) {
					res = true;
				}
			}
		}
		if ( not res )
			std::cerr << "Can't find variable '" << name  << "' : "  << self->toJson() << std::endl;
	}
	return self->reconstruction(pass, variables) && res;
}

llvm::Value* ObjectElement::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	return builder.CreateLoad(getAddr(builder, context, module)); // because of vtable is first
}
