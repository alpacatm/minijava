#include "BooleanNode.h"
#include "llvm/IR/TypeBuilder.h"

llvm::Value* BooleanNode::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	return builder.getInt1(value);
}