#include "Lexer.hh"
#include <cstdlib>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <memory>

#pragma GCC diagnostic ignored "-Wsign-compare"

std::stringstream strbuf;

%%{
	machine Lexer;
	alphtype unsigned long;
	write data;

	number = digit+;

	ws = [ \t\n];

	qqstr := |*
		'\\'["nt\\] => {
			switch ( ts[1] ) {
				case 'n': strbuf << "\n"; break;
				case 't': strbuf << "\t"; break;
				default:  strbuf << std::string(ts + 1, te);
			}
		};

		[^"\\] => {
			strbuf << std::string(ts, te);
		};

		'"' => {
			ret = Parser::token::STRING_VALUE;
			val->str = new string(strbuf.str());
			fnext main;
			fbreak;
		};
	*|;

	qstr := |*
		'\\'['\\] => {
			strbuf << std::string(ts + 1, te);
		};

		'\\' | [^'\\] => {
			strbuf << std::string(ts, te);
		};

		"'" => {
			ret = Parser::token::STRING_VALUE;
			val->str = new string(strbuf.str());
			fnext main;
			fbreak;
		};
	*|;

	main := |*

		number => {
			ret = Parser::token::INTEGER_VALUE;
			val->integer = atoi(std::string(ts, te).c_str());
			fbreak;
		};

		'if'    => { ret = Parser::token::IF; fbreak; };
		'elsif' => { ret = Parser::token::ELSIF; fbreak; };
		'else'  => { ret = Parser::token::ELSE; fbreak; };
		'while'  => { ret = Parser::token::WHILE; fbreak; };
		'new'    => { ret = Parser::token::NEW; fbreak; };
		'return' => { ret = Parser::token::RETURN; fbreak; };

		'true'   => {
			ret = Parser::token::BOOLEAN_VALUE;
			val->boolean = true;
			fbreak;
		};
		'false'  => {
			ret = Parser::token::BOOLEAN_VALUE;
			val->boolean = false;
			fbreak;
		};
		'this'   => { ret = Parser::token::THIS; fbreak; };

		'public' => { ret = Parser::token::PUBLIC; fbreak; };
		'class' => { ret = Parser::token::CLASS; fbreak; };
		'extends' => { ret = Parser::token::EXTENDS; fbreak; };
		'native' => { ret = Parser::token::NATIVE; fbreak; };

		'use' => { ret = Parser::token::USE; fbreak; };

        'fun' => { ret = Parser::token::FUN; fbreak; };

		'"' => {
			strbuf.str("");
			fgoto qqstr;
		};


		"'" => {
			strbuf.str("");
			fgoto qstr;
		};


		 ((0x0400..0x04FF) | [a-zA-Z0-9_])+ => {
		  val->str = new std::string(ts, te);
		  ret = Parser::token::IDENTIFIER; fbreak;
		 };

		'{' => { ret = Parser::token::LCURVE; fbreak; };
		'}' => { ret = Parser::token::RCURVE; fbreak; };

		'(' => { ret = Parser::token::LPAREN; fbreak; };
		')' => { ret = Parser::token::RPAREN; fbreak; };

		'+' => { ret = Parser::token::PLUS; fbreak; };
		'-' => { ret = Parser::token::MINUS; fbreak; };

		'*' => { ret = Parser::token::MUL; fbreak; };
		'/' => { ret = Parser::token::DIV; fbreak; };
		'%' => { ret = Parser::token::MOD; fbreak; };
		'**' => { ret = Parser::token::POW; fbreak; };

		'&&' => { ret = Parser::token::AND; fbreak; };
		'||' => { ret = Parser::token::OR; fbreak; };
		'!' => { ret = Parser::token::NOT; fbreak; };
		'<' => { ret = Parser::token::LE; fbreak; };
		'<=' => { ret = Parser::token::LEQ; fbreak; };
		'==' => { ret = Parser::token::EQ; fbreak; };
		'!=' => { ret = Parser::token::NE; fbreak; };
		'>=' => { ret = Parser::token::GEQ; fbreak; };
		'>' => { ret = Parser::token::GE; fbreak; };

		'[' => { ret = Parser::token::LSQUARE; fbreak; };
		']' => { ret = Parser::token::RSQUARE; fbreak; };
		'[' ws* ']' => { ret = Parser::token::LRSQUARE; fbreak; };

		';'+ => { ret = Parser::token::DELIM; fbreak; };
		',' => { ret = Parser::token::COMMA; fbreak; };
		'.' => { ret = Parser::token::DOT; fbreak; };
		':' => { ret = Parser::token::COLON; fbreak; };

		'=' => { ret = Parser::token::ASSIGN; fbreak; };

		ws;

		'//' [^\n]+;

	*|;
}%%

minijava::Lexer::Lexer (char const* p_, char const* pe_): p(p_),pe(pe_),eof(pe_) {
	%% write init;
}


minijava::Parser::token_type minijava::Lexer::lex(Parser::semantic_type* val) {
	Parser::token_type ret = Parser::token::END;
	%% write exec;
	return ret;
}
