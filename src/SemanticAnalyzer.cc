#include "SemanticAnalyzer.h"
#include "ControlFlow.h"
#include "FunNode.h"
#include "ReturnNode.h"

bool SemanticAnalyzer::checkNode(shared_ptr <AstNode> node) {
	node->getType();
	bool result = true;
	for (auto child : node->getChildrenForTypeCheck()) {
		if (not checkNode(child)) result = false;
	}
	if ( auto func = std::dynamic_pointer_cast<FunNode>(node) ) {
		list<shared_ptr<ReturnNode>> returns;
		auto good = true;
		if ( auto flow = std::dynamic_pointer_cast<ControlFlow>(func->body) ) {
			returns = flow->getReturns();
			for ( auto ret : returns ) {
				if ( *(ret->getType()) != *(func->getType()) ) {
					std::cerr << "Bad result type for '" << func->getName() << "' from " << ret->toJson() << std::endl;
					good = false;
				}
			}
			if ( !func->isProcedure() && !func->native && !flow->isTerminator() ) {
				std::cerr << "There is possible no result from '" << func->getName() << "'" << std::endl;
				good = false;
			}
		} else if ( !func->isProcedure() && !func->native && !returns.size() ) {
			std::cerr << "There is possible no result from '" << func->getName() << "'" << std::endl;
			good = false;
		}

		result = good && result;
	}
	return result;
}