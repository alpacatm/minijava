#include "BlockNode.h"
#include "ReturnNode.h"

BlockNode::BlockNode()
{}

BlockNode::BlockNode(const vector<shared_ptr<AstNode>>& exprs):
	body(exprs)
{}

string BlockNode::toJson() const {
	stringstream sexprs;
	for ( auto expr : body )
		sexprs << expr->toJson() << ",";
	return "{'block' : [" + sexprs.str() + "]}";
}

shared_ptr<Type> BlockNode::getType() const {
	return shared_ptr<Type>(new VoidType());
}

vector<shared_ptr<AstNode>> BlockNode::getChildrenForTypeCheck() const {
	return body;
}

bool BlockNode::_reconstruction_action(int pass, std::map<string,string>& variables) {
	auto res = true;
	for ( auto expr : body )
		res = expr->reconstruction(pass, variables) && res;
	return res;
}

llvm::Value* BlockNode::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	llvm::Value* res = 0; // TODO: make it void
	for ( auto inner : body ) {
		res = inner->codegen(builder, context, module);
		if ( isPrevBlocTerminator(builder) )
			return res;
	}
	return res;
}

bool BlockNode::isTerminator() {
	for ( auto inner : body )
		if ( auto flow = std::dynamic_pointer_cast<ControlFlow>(inner) )
			if ( flow->isTerminator() )
				return true;
	return false;
}

list<shared_ptr<ReturnNode>> BlockNode::getReturns() {
	list<shared_ptr<ReturnNode>> result;
	for ( auto inner : body )
		if ( auto flow = std::dynamic_pointer_cast<ControlFlow>(inner) ) {
			auto some = flow->getReturns();
			result.splice(result.begin(), some);
		}
	return result;
}