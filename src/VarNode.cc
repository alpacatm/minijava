#include "VarNode.h"
#include "Namespace.h"
#include "SemanticAnalyzer.h"

llvm::Value* VarNode::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	return builder.CreateLoad(addr);
}

bool VarNode::_reconstruction_action(int pass, std::map<string,string>& variables) {
	if ( type->name != JUNK_TYPE->name )
		return type->reconstruction(pass, variables);
	else if ( pass > THIRD_PASS ) {
		std::cerr << "Undefined variable \"" << name << "\"" << std::endl;
		return false;
	} else {
		return true;
	}
}