#include <algorithm>
#include <cstdlib>


#include "UsageNode.h"
#include "SemanticAnalyzer.h"
#include "Module.h"

std::map<string, shared_ptr<AstNode>> UsageNode::cache;

bool UsageNode::_reconstruction_action(int pass, std::map<string,string>& variables) {
	auto res = true;

	if ( pass == FIRST_PASS ) {
		if ( cache.find(module) == cache.end() ) {
			try {
				cache[module] = Module::findByName(module);
				for ( int sub_pass = FIRST_PASS; (sub_pass <= pass) && res; sub_pass++ )
					res = cache[module]->reconstruction(sub_pass, variables);
			} catch (const std::exception& exc) {
				std::cerr << "Can't load module '" << module << "': " << exc.what() << std::endl;
				res = false;
			}
		}
	} else if ( (res = cache[module]->reconstruction(pass, variables)) ) {
		if ( pass == SECOND_PASS ) {
			res = std::dynamic_pointer_cast<Module>(cache[module])->exportTypes(target_namespace);
		} else if ( pass == POST_LAST ) {
			res = std::dynamic_pointer_cast<Module>(cache[module])->compile();
		}
	}
	return res;
}

void UsageNode::fillUsages(std::set<string>& usages) {
	std::dynamic_pointer_cast<Module>(cache[module])->fillUsages(usages);
}