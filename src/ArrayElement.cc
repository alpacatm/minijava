
#include "ArrayElement.h"

ArrayElement::ArrayElement(const shared_ptr<AstNode>& elem, const shared_ptr<AstNode>& arr):
	Addressable(""),
	el(elem),
	array(arr) {}

string ArrayElement::toJson() const {
	return "{'el' : " + el->toJson() + ", 'array': " + array->toJson() + "}";
}

shared_ptr<Type> ArrayElement::getType() const {
	auto array_type = std::dynamic_pointer_cast<ArrayType>(array->getType());
	if (!array_type)
		throw std::runtime_error("Type of array '" + array->getType()->name + "' is not an array");
	return array_type->elementType->getType();
}

vector<shared_ptr<AstNode>> ArrayElement::getChildrenForTypeCheck() const {
	return {array, el};
}

bool ArrayElement::_reconstruction_action(int pass, std::map<string,string>& variables) {
	bool res = el->reconstruction(pass, variables);
	return array->reconstruction(pass, variables) && res;
}

llvm::Value* ArrayElement::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	return builder.CreateLoad(getAddr(builder, context, module));
}

llvm::Value* ArrayElement::getAddr(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	auto pointer_to_array = array->codegen(builder, context, module);
	auto element_index = el->codegen(builder, context, module);

	auto arr_ptr = builder.CreateStructGEP( pointer_to_array, 1 );
	addr = builder.CreateInBoundsGEP( builder.CreateLoad(arr_ptr), element_index );
	return addr;
}
