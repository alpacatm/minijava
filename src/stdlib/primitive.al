class array {
	int length;

	public fun this(int len) {
		length = len;
	}

	public fun len() : int {
		return length;
	}
}

native class ustring {
	public fun this() native // never used, rename to private, or maybe there will be no default constructor for native classes

	public fun toString() : String {
		return String(this);
	}
}