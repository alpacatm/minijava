use primitive;

// move it to primitive after adding support of selective usages

native class boolean {
	public fun this() native

	public fun toString() : String {
		return String(this._toUstring());
	}

	public fun !() : boolean native

	public fun &&(boolean other) : boolean native
	public fun ||(boolean other) : boolean native

	// TODO make it private
	public fun _toUstring() : ustring native
}

native class int {
	public fun this() native

	public fun toString() : String {
		return String(this._toUstring());
	}

	public fun *(int other) : int native
	public fun +(int other) : int native
	public fun -(int other) : int native
	public fun /(int other) : int native
	public fun %(int other) : int native
	public fun **(int other) : int native

	public fun <(int other)  : boolean native
	public fun <=(int other) : boolean native
	public fun >=(int other) : boolean native
	public fun >(int other) : boolean native
	public fun ==(int other) : boolean native
	public fun !=(int other) : boolean native

	public fun -() : int native
	public fun +() : int native

	// TODO make it private
	public fun _toUstring() : ustring native
}