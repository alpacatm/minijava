#include <gc.h>

// Boehm garbage collector

void* bgc_malloc(size_t size) {
	return GC_malloc(size);
}

void bgc_init() {
	GC_INIT();
}