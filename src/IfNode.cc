#include "IfNode.h"
#include "ReturnNode.h"


bool IfNode::_reconstruction_action(int pass, std::map<string,string>& variables) {
	auto res = condition->reconstruction(pass, variables);
	res = action->reconstruction(pass, variables) && res;
	res = (otherwise ? otherwise.get()->reconstruction(pass, variables) : true) && res;
	return res;
}

llvm::Value* IfNode::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	if ( isPrevBlocTerminator(builder) )
		return 0;

	auto cond = condition->codegen(builder, context, module);

	auto block = builder.GetInsertBlock()->getParent();

	auto thenb = llvm::BasicBlock::Create(context, "then", block);
	auto elseb = llvm::BasicBlock::Create(context, "else");
	auto endifb = llvm::BasicBlock::Create(context, "endif");

	builder.CreateCondBr(cond, thenb, elseb);

	builder.SetInsertPoint(thenb);
	/*auto thenv = */action->codegen(builder, context, module);
	if ( not isPrevBlocTerminator(builder) )
		builder.CreateBr(endifb);
	/*thenb = */builder.GetInsertBlock();

	block->getBasicBlockList().push_back(elseb);
	builder.SetInsertPoint(elseb);
	//llvm::Value* elsev = 0;
	if ( otherwise )
		/*elsev = */otherwise.get()->codegen(builder, context, module);

	if ( not isPrevBlocTerminator(builder) )
		builder.CreateBr(endifb);
	/*elseb = */builder.GetInsertBlock();

	block->getBasicBlockList().push_back(endifb);
	builder.SetInsertPoint(endifb);

	// TODO this code for return values from if, use it when you can return values
	//auto phi = builder.CreatePHI(llvm::IntegerType::get(context, 32), 2);
	//phi->addIncoming(thenv, thenb);
	//phi->addIncoming(elsev, elseb);
	//return phi;
	return 0;
}


bool IfNode::isTerminator() {
	if ( otherwise )
		if ( auto actionFlow = std::dynamic_pointer_cast<ControlFlow>(action) )
			if ( auto elseFlow = std::dynamic_pointer_cast<ControlFlow>(otherwise.get()) )
				return actionFlow->isTerminator() && elseFlow->isTerminator();
	return false;
}

list<shared_ptr<ReturnNode>> IfNode::getReturns() {
	list<shared_ptr<ReturnNode>> result;
	if ( auto actionFlow = std::dynamic_pointer_cast<ControlFlow>(action) )
		result = actionFlow->getReturns();
	if ( auto elseFlow = std::dynamic_pointer_cast<ControlFlow>(otherwise.get()) ) {
		auto some = elseFlow->getReturns();
		result.splice(result.begin(), some);
	}
	return result;
}