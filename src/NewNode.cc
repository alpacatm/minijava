#include "NewNode.h"
#include "ObjectType.h"
#include "SemanticAnalyzer.h"


NewNode::NewNode(const shared_ptr<TypeNode>& ttype, const vector<shared_ptr<AstNode>>& pars):
	type(ttype), params(pars)
{}

string NewNode::toJson() const {
	stringstream sparams;
	for ( auto par : params )
		sparams << par->toJson() << ",";

	if ( std::dynamic_pointer_cast<ObjectType>(type->getType()) )
		return "{'new' : '" + type->name + "', 'params': [" + sparams.str() + "]}";
	else
		return "{'new' : " + type->toJson() + ", 'params': [" + sparams.str() + "]}";
}

shared_ptr<Type> NewNode::getType() const {
	return type->getType();
}

vector<shared_ptr<AstNode>> NewNode::getChildrenForTypeCheck() const {
	return params;
}


bool NewNode::_reconstruction_action(int pass, std::map<string,string>& variables) {
	auto res = true;

	for ( auto par : params )
		res = par->reconstruction(pass, variables) && res;

	if ( pass == FOURTH_PASS ) {
		if ( auto klass = std::dynamic_pointer_cast<ObjectType>(type->getType()) ) {
			constructor = klass->findFunction(params, [](const shared_ptr<FunNode>& fn) { return fn->constructor; });
			if ( not constructor ) {
				std::cerr << "There is no constructor for " << this->toJson() << std::endl;
				res = false;
			}
		}
	}

	if (constructor != nullptr) constructor->reconstruction(pass, variables);

	return res;
}

llvm::Value* NewNode::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	auto ntype = this->type->getType()->getLLVMType(builder, context, module);
	auto depointed = static_cast<llvm::PointerType*>(ntype);

	auto malloc = NewNode::getMalloc(context, module);

	llvm::DataLayout* dl = new llvm::DataLayout(module);
	auto allocated = builder.CreateCall(
		malloc,
		builder.getInt64( dl->getTypeAllocSize(depointed->getElementType()) )
	);

	llvm::Value* tmp = builder.CreateBitCast(allocated, depointed, "mallocated");

	std::vector<llvm::Value*> args = { tmp };
	for (auto param : params) {
		args.push_back(param->codegen(builder, context, module));
	}

	if ( constructor ) {
		string cname = this->type->name + "." + this->constructor->getRawSignature();
		auto cfunc = constructor->getLLVMSignature(builder, context, module);
		auto constr = module->getOrInsertFunction(cname, cfunc->getFunctionType());
		builder.CreateCall(constr, args);
	} else {
		string cname = this->type->name + "." + this->type->name;
		auto cfunc = module->getFunction(cname);
		auto constr = module->getOrInsertFunction(cname, cfunc->getFunctionType());
		builder.CreateCall(constr, args);
	}
	return tmp;
}