#include "AssignNode.h"

AssignNode::AssignNode(const shared_ptr<Addressable> lvalue, const shared_ptr<AstNode> rvalue) :
	var(lvalue), expr(rvalue)
{}

string AssignNode::toJson() const {
	return "{'assign' : " + var->toJson() + ", 'rvalue': " + expr->toJson() + "}";
}

shared_ptr<Type> AssignNode::getType() const {
	if (not expr->getType())
		throw std::runtime_error("Cannot assign expression of UNKNOWN type to variable of type '"
			+ var->getType()->name + "'");

	if (*expr->getType() != *var->getType()) {
		throw std::runtime_error("Cannot assign expression of type '"
			+ expr->getType()->name + "' to variable of type '" + var->getType()->name + "'");
	}

	return var->getType();
}

vector<shared_ptr<AstNode>> AssignNode::getChildrenForTypeCheck() const {
	return {var, expr};
}

llvm::Value* AssignNode::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	auto value = expr->codegen(builder, context, module);
	auto addr = builder.CreateBitCast(
		var->getAddr(builder, context, module),
		llvm::PointerType::getUnqual(expr->getType()->getLLVMType(builder, context, module))
	);
	return builder.CreateStore(value, addr);
}


bool AssignNode::_reconstruction_action(int pass, std::map<string,string>& variables) {
	auto res = var->reconstruction(pass, variables);
	return expr->reconstruction(pass, variables) && res;
}
