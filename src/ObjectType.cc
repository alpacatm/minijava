#include <set>
#include <map>

#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/DerivedTypes.h"

#include "ObjectType.h"
#include "SemanticAnalyzer.h"
#include "AstNode.h"
#include "BlockNode.h"
#include "ReturnNode.h"
#include "IntegerNode.h"
#include "VoidType.h"

ObjectType::ObjectType(const string& name, const vector<shared_ptr<VarNode>>& class_vars,
		   const vector<shared_ptr<FunNode>>& class_methods,
		   const optional<shared_ptr<TypeNode>>& class_super,
		   const bool isExternal) :
	Type(name),
	variables(class_vars),
	methods(class_methods),
	super(class_super),
	external(isExternal)
{
	for ( auto method : methods )
		method->owner_class = this;
}

string ObjectType::toJson() const {
	stringstream svars;
	for ( auto def : variables )
		svars << def->toJson() << ",";
	stringstream smeths;
	for ( auto def : methods )
		smeths << def->toJson() << ",";
	return "{'class' : '" + name
			+ "', 'variables':[" + svars.str()
			+ "], 'methods':[" + smeths.str() + "]"
			+ (super ? ", 'super': '" + super.get()->name  + "'" : "")
			+ "}";
}

vector<shared_ptr<AstNode>> ObjectType::getChildrenForTypeCheck() const {
	vector<shared_ptr<AstNode>> children {};
	for (auto child : variables) {
		children.push_back(std::dynamic_pointer_cast<AstNode>(child));
	}
	for (auto child : methods) {
		children.push_back(std::dynamic_pointer_cast<AstNode>(child));
	}
	return children;
}

bool ObjectType::_reconstruction_action(int pass, std::map<string,string>& rvariables) {
	auto res = true;

	if ( pass == THIRD_PASS ) {
		auto rpos = methods.size() - 1;
		for ( size_t pos = 0; pos < methods.size(); pos++ )
			if ( methods[pos]->constructor || methods[pos]->entrypoint )
				for ( ; rpos >= pos; rpos-- ) {
					if ( rpos == pos ) {
						pos = methods.size();
						break;
					} else if ( not ( methods[rpos]->constructor || methods[rpos]->entrypoint ) ) {
						auto tmp = methods[rpos];
						methods[rpos] = methods[pos];
						methods[pos] = tmp;
					}
				}

		// check for looped-inheritance
		if ( auto parent = super ) {
			std::set<Type*> ancestry;
			while ( parent && res ) {
				auto naked = parent.get()->getType().get();
				if ( not naked ) {
					std::cerr << "Unknown type '" << parent.get()->name << "' as ancestor of '" << this->name << "'" << std::endl;
					res = false;
				} else if ( ancestry.count(naked) ) {
					std::cerr << "Inheritance loop started from " << name << " and with " << naked->name << std::endl;
					res = false;
				} else {
					ancestry.insert(naked);
					parent = ((ObjectType*) naked)->super; // TODO: We'll have only object types in future. need brushy
				}
			}
	
			if ( res ) {
				// reconstruct parent
				res = super.get()->reconstruction(pass, rvariables);
	
				// reconstruct methods
				std::map<string, pair<size_t, shared_ptr<FunNode>>> my_methods;
				for ( size_t pos = 0; pos < methods.size(); pos++ )
					my_methods[ methods[pos]->getName() ] = std::make_pair(pos, methods[pos]);

				auto parent = std::dynamic_pointer_cast<ObjectType>(super.get()->getType());

				auto super_methods = parent->methods;
				for ( size_t pos = 0; pos < super_methods.size(); pos++ ) {
					auto super_method = super_methods[pos];
					if ( super_method->constructor || super_method->entrypoint ) break;
					auto foundo = my_methods.find(super_method->getName());
					if ( foundo != my_methods.end() ) {
						auto self_method = foundo->second;
						if ( self_method.first != pos ) {
							auto tmp = methods[pos];
							methods[pos] = self_method.second;
							methods[self_method.first] = tmp;
						}
					} else {
						methods.insert(methods.begin() + pos, super_method);
					}
				}
	
				// reconstruct variables
				std::map<string, pair<size_t, shared_ptr<VarNode>>> my_vars;
				for ( size_t pos = 0; pos < variables.size(); pos++ )
					my_vars[ variables[pos]->name ] = std::make_pair(pos, variables[pos]);
				
				auto super_vars = parent->variables;
				for ( size_t pos = 0; pos < super_vars.size(); pos++ ) {
					auto super_var = super_vars[pos];
					auto foundo = my_vars.find(super_var->name);
					if ( foundo != my_vars.end() ) {
						auto self_var = foundo->second;
						if ( self_var.first != pos ) {
							auto tmp = variables[pos];
							variables[pos] = self_var.second;
							variables[self_var.first] = tmp;
						}
					} else {
						variables.insert(variables.begin() + pos, super_var);
					}
	
					variables[pos]->type = super_var->type;
				}
			}
		}
		if ( not (external || this->can_be_main()) ) {
			auto exists_constructor = false;
			for ( auto meth : methods )
				if ( meth->constructor)
					exists_constructor = true;
			if ( not exists_constructor ) {
				auto constructor = shared_ptr<FunNode>(new FunNode("", false, true));
				constructor->rtype = shared_ptr<TypeNode>(new TypeNode(shared_ptr<Type>(new VoidType())));
				constructor->args = {};
				constructor->body = shared_ptr<BlockNode>(new BlockNode({
					shared_ptr<ReturnNode>(new ReturnNode(optional<shared_ptr<AstNode>>()))
				}));
				constructor->owner_class = this;
				constructor->getName();
				this->methods.push_back(constructor);
			}
		}
	} else if ( super )
		res = super.get()->reconstruction(pass, rvariables) && res;

	for ( auto def : methods )
		res = def->reconstruction(pass, rvariables) && res;

	return res;
}

llvm::Value* ObjectType::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	this->getLLVMType(builder, context, module);

	llvm::Value* res = 0;
	if ( not external )
		for ( auto meth : methods )
			res = meth->codegen(builder, context, module);

	return res;
}

llvm::Type* ObjectType::getLLVMType(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) const {
	// TODO maybe cache?
	if ( native ) {
		if ( nativeType->isIntegerTy() ) { // TODO make real copy!
			if ( not module->getTypeByName(this->name) ) {
				llvm::StructType::create(context, name);
			}
			return llvm::Type::getIntNTy(context, nativeType->getIntegerBitWidth());
		}
		return llvm::Type::getIntNPtrTy(context, 8); // TODO remove
	}
	auto res = module->getTypeByName(this->name);
	if ( not res )
		res = llvm::StructType::create(context, name);

	if ( res->isOpaque() && !( external || native ) ) { // we can define opaque type in native code
		auto vtable_name = name + "..vtable";
		auto vtable = llvm::StructType::create(context, vtable_name);
		vector<llvm::Type*> class_content = { llvm::PointerType::getUnqual(vtable)};

		res->setBody(class_content); // stop deep recursion

		vector<llvm::Type*> vmethods;
		for ( auto meth : methods )
			if ( not meth->entrypoint ) {
				auto fun = meth->getLLVMSignature(builder, context, module);
				if ( not meth->constructor )
					vmethods.push_back(llvm::PointerType::getUnqual(fun->getFunctionType()));
			}

		vtable->setBody(vmethods);

		for ( auto var : variables ) {
			class_content.push_back(var->getType()->getLLVMType(builder, context, module));
		}
		res->setBody(class_content);


		vector<llvm::Constant*> in;
		for ( auto meth : methods )
			if ( not (meth->entrypoint || meth->constructor) ) {
				auto fun = meth->getLLVMSignature(builder, context, module);
				in.push_back(module->getOrInsertFunction(fun->getName(), fun->getFunctionType()));
			}

		// vtable content
		new llvm::GlobalVariable(
		/*Module=*/*module,
		/*Type=*/vtable,
		/*isConstant=*/true,
		/*Linkage=*/llvm::GlobalValue::PrivateLinkage,
		/*Initializer=*/llvm::ConstantStruct::get(vtable, in), // has initializer, specified below
		/*Name=*/vtable_name + "..data"
		);
	}

	return native ? ((llvm::Type*) res) : ((llvm::Type*) llvm::PointerType::getUnqual(res));
}

// TODO : i think, we can cache it
llvm::Type* ObjectType::getLLVMLiteralType(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) const {
	vector<llvm::Type*> vmethods;
	for ( auto meth : methods )
		if ( not meth->entrypoint ) {
			auto fun = meth->getLLVMSignature(builder, context, module);
			if ( not meth->constructor )
				vmethods.push_back(llvm::PointerType::getUnqual(fun->getFunctionType()));
		}

	vector<llvm::Type*> class_content = { llvm::PointerType::getUnqual(llvm::StructType::get(context, vmethods)) };

	for ( auto var : variables )
		class_content.push_back(var->getType()->getLLVMType(builder, context, module));

	return llvm::PointerType::getUnqual(llvm::StructType::get(context, class_content));
}

shared_ptr<ObjectType> ObjectType::asExternal() {
	vector<shared_ptr<FunNode>> newMethods;
	for ( auto meth : methods ) {
		newMethods.emplace_back(new FunNode(*meth));
	}
	auto res =  shared_ptr<ObjectType>(new ObjectType(name, variables, newMethods, super, true));

	// TODO: move it to constructor
	res->native = native;
	res->nativeType = nativeType;

	return res;
}