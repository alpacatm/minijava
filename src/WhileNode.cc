#include "WhileNode.h"
#include "ReturnNode.h"


WhileNode::WhileNode(const shared_ptr<AstNode>& exp, const shared_ptr<AstNode>& st) :
	expression(exp),
	statement(st)
{}

string WhileNode::toJson() const {
	return "{'while' : " + expression->toJson() + ", 'do':" + statement->toJson() + "}";
}

shared_ptr<Type> WhileNode::getType() const {
	return shared_ptr<Type>(new VoidType());
}

vector<shared_ptr<AstNode>> WhileNode::getChildrenForTypeCheck() const {
	return {expression, statement};
}

bool WhileNode::_reconstruction_action(int pass, std::map<string,string>& variables) {
	auto res = expression->reconstruction(pass, variables);
	return statement->reconstruction(pass, variables) && res;
}

llvm::Value* WhileNode::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	if ( isPrevBlocTerminator(builder) )
		return 0;

	auto block = builder.GetInsertBlock()->getParent();

	auto pre_loopb = llvm::BasicBlock::Create(context, "pre_loop", block);
	auto thenb = llvm::BasicBlock::Create(context, "loop_body");
	auto elseb = llvm::BasicBlock::Create(context, "end_loop");

	builder.CreateBr(pre_loopb);
	builder.SetInsertPoint(pre_loopb);
	
	auto cond = expression->codegen(builder, context, module);

	builder.CreateCondBr(cond, thenb, elseb);

	block->getBasicBlockList().push_back(thenb);
	builder.SetInsertPoint(thenb);
	statement->codegen(builder, context, module);

	if ( not isPrevBlocTerminator(builder) )
		builder.CreateBr(pre_loopb);

	block->getBasicBlockList().push_back(elseb);
	builder.SetInsertPoint(elseb);

	return 0; // TODO : make it void
}


list<shared_ptr<ReturnNode>> WhileNode::getReturns() {
	list<shared_ptr<ReturnNode>> result;
	if ( auto statementFlow = std::dynamic_pointer_cast<ControlFlow>(statement) )
		result = statementFlow->getReturns();
	return result;
}