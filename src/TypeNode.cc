#include "TypeNode.h"

#include "SemanticAnalyzer.h"

bool TypeNode::_reconstruction_action(int pass, std::map<string,string>& variables) {
	if ( type )
		return type->reconstruction(pass, variables);
	else if ( pass > SECOND_PASS ) {
		std::cerr << "Undefined type '" << name <<"'" << std::endl;
		return false;
	} else {
		return true;
	}
}