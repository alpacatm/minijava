#include "ReturnNode.h"

bool ReturnNode::_reconstruction_action(int pass, std::map<string,string>& variables) {
	auto res = true;
	if ( ret_expr )
		res = ret_expr.get()->reconstruction(pass, variables);
	return res;
}

llvm::Value* ReturnNode::codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
	if ( isPrevBlocTerminator(builder) )
		return 0;
	auto curf = builder.GetInsertBlock()->getParent();
	llvm::Value* result = 0;
	if ( ret_expr ) {
		result = ret_expr.get()->codegen(builder, context, module);
		builder.CreateStore(result, curf->getValueSymbolTable().lookup("@result"));
	}

	llvm::BasicBlock* ret = 0;
	for (llvm::BasicBlock& block : curf->getBasicBlockList()) {
		if ( block.getName() == "@return" )
			ret = &block;
	}
	builder.CreateBr(ret);
	return result;
}
