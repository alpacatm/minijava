#!/usr/bin/perl
use 5.010;
use strict;
use warnings;

use Test::More;
use Cwd 'abs_path';
use YAML::XS;
use File::Slurp;
use Getopt::Long;

use Data::Dumper;

use lib (abs_path($0) =~ m@^(.*)/[^/]+$@);
use Wrap qw(wrap);

(my $ABS = abs_path($0)) =~ s@/[^/]+$@@;
my $DEBUG = 0;
my $FILES = "";
my $bin_dir = "$ABS/../";
my $tests = "*";
GetOptions ("debug" => \$DEBUG, "file=s" => \$FILES, "binary-dir=s" => \$bin_dir, "tests=s" => \$tests);

foreach my $fixture ( (not $FILES eq "" and (<$ABS/parsing/$FILES>)) or <$ABS/parsing/$tests.yml> ) {
	(my $relative = $fixture) =~ s@\Q$ABS/\E@@;
	say "\n$relative";
	if ( my $ymls = eval { [Load(scalar(read_file($fixture)))] } ) {
		foreach my $yml ( @$ymls ) {
			unless ( $yml ) {
				warn "Some empty test in $fixture";
				next;
			}
			
			$yml = wrap($yml) if $yml->{wrapper};

			open(my $tfh, "-|", "$bin_dir/minijavac", "-p", "-i", $yml->{in})
			or die "Can't open: $!";

			my $raw = join("", <$tfh>);
			if ( my $res = eval { Load($raw) } ) {
				is_deeply($res, $yml->{out}, $yml->{name})
				or $DEBUG and print("FOR:\n$yml->{in}\nPARSED:\n" . Dumper($res) . "\nEXPECTED:\n".Dumper($yml->{out}));
			} else {
				fail("[$yml->{name}]".": Can't parse '$yml->{in}' '$raw': $@");
			}
		}
	} else {
		fail("Can't parse $fixture: $@");
	}
}

done_testing();