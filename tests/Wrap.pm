package Wrap;

use 5.010;
use strict;
use warnings;
no warnings "experimental::smartmatch";


use YAML::XS;
use File::Slurp;
use Cwd 'abs_path';

use base "Exporter";

our @EXPORT_OK = qw(wrap);


sub wrap {
	state $wrappers = do {
		(my $abs_path =  abs_path(__FILE__)) =~ s@/[^/]+$@@;
		eval { Load(scalar(read_file("$abs_path/wrapper.yml"))) };
	};
	my $yml = shift;
	my $options = shift;
	if ( my $wrap = $wrappers->{$yml->{wrapper}} ) {
		my %result = %$yml;
		($result{in} = $wrap->{in}) =~ s/%%in%%/$yml->{in}/g if $options->{wrap_in} // 1;
		$result{out} = substitute($wrap->{out}, $yml->{out}) if $options->{wrap_out} // 1;
		return \%result;
	} else {
		warn "There is no wrapper $yml->{wrapper}";
		return $yml;
	}
}

sub substitute {
	my $node = shift;
	my $insert = shift;
	given ( ref($node) ) {
		when ( 'HASH' ) {
			return { map { $_ => substitute($node->{$_}, $insert) } keys $node };
		}
		when ( 'ARRAY' ) {
			return [ map { substitute($_, $insert) } @$node ];
		}
		defaul {
			return $node eq '%%out%%' ? $insert : $node;
		}
	}
}

1;