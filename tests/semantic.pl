#!/usr/bin/perl
use 5.010;
use strict;
use warnings;

use Test::More;
use Cwd 'abs_path';
use YAML::XS;
use File::Slurp;
use Getopt::Long;
use IPC::Open3;
use IO::Handle;
use Symbol 'gensym';

use lib (abs_path($0) =~ m@^(.*)/[^/]+$@);
use Wrap qw(wrap);

use Data::Dumper;

(my $abs = abs_path($0)) =~ s@/[^/]+$@@;
my $DEBUG = 0;
my $bin_dir = "$abs/../";
my $tests = "*";
GetOptions ("debug" => \$DEBUG, "binary-dir=s" => \$bin_dir, "tests=s" => \$tests);

foreach my $fixture ( <$abs/semantic/$tests.yml> ) {
	(my $relative = $fixture) =~ s@\Q$abs/\E@@;
	say "\n$relative";
	if ( my $ymls = eval { [Load(scalar(read_file($fixture)))] } ) {
		foreach my $yml ( @$ymls ) {
			unless ( $yml ) {
				warn "Some empty test in $fixture";
				next;
			}

			$yml = wrap($yml, {wrap_out => 0}) if $yml->{wrapper};

			my $err = gensym;
			my $pid = open3(my $wtr, my $rdr, $err, "$bin_dir/minijavac", "-ai", $yml->{in}) or die "Can't open: $!";

			waitpid( $pid, 0 );
			my $child_exit_status = $? >> 8;

			my $raw = join("", <$err>);
			$raw =~ s/\n/" "/;

			$wtr->close;
			$rdr->close;
			$err->close;

			if ($yml->{out} eq "no-exception") {
				ok( $raw eq "", $yml->{name})
				or $DEBUG
				and diag("$yml->{name}: $yml->{in}\nExpected no exception\nCaught stderr: $raw");
			} else {
				ok( index($raw, $yml->{out}) >= 0, $yml->{name})
				or $DEBUG
				and diag("$yml->{name}: $yml->{in}\nExpected exception: $yml->{out}\nCaught stderr: $raw");
			}
		}
	} else {
		fail("Can't parse $fixture: $@");
	}
}

done_testing();
