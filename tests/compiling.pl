#!/usr/bin/perl

use strict;
use warnings;
use 5.010;

use POSIX ":sys_wait_h";

use Test::More;
use Cwd 'abs_path';
use YAML::XS;
use File::Slurp;
use Getopt::Long;
use File::Temp qw/tempfile/;

use Data::Dumper;

(my $abs = abs_path($0)) =~ s@/[^/]+$@@;
my $DEBUG = 0;
my $bin_dir = "$abs/../";
my $tests = "*";
GetOptions ("debug" => \$DEBUG, "binary-dir=s" => \$bin_dir, "tests=s" => \$tests);

my $target = @ARGV ? join("|", map { qr/(?:\Q$_\E)/ } @ARGV) : qr/.*/;
$target = qr/$target/;

foreach my $fixture ( <$abs/compiling/$tests.yml> ) {
	(my $relative = $fixture) =~ s@\Q$abs/\E@@;
	next unless $relative =~ $target;
	say "\n$relative";
	if ( my $ymls = eval { [Load(scalar(read_file($fixture)))] } ) {
		foreach my $yml ( @$ymls ) {
			unless ( $yml ) {
				warn "Some empty test in $fixture";
				next;
			}

			my ($fh, $filename) = tempfile("minijavaXXXXXXX", DIR => "/tmp");
			close $fh;

			if ( system("$bin_dir/minijavac", "-c", "-i", $yml->{in}, "-o", $filename) ) {
				fail("Can't call compiler: $!");
				next;
			}

			my $rfh;
			unless ( open($rfh, "-|", $filename) ) {
				fail("Can't run code: $!");
				next;
			}

			my $res = join("", <$rfh>);

			is($res, $yml->{out}, "$yml->{name} :: $filename");
		}
	} else {
		fail("Can't parse $fixture: $@");
	}
}

done_testing();