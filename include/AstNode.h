#pragma once

#include <memory>
#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <iostream>
#include <list>
#include <boost/optional.hpp>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "Type.h"

using std::string;
using std::list;
using std::vector;
using std::pair;
using boost::optional;
using std::weak_ptr;
using std::shared_ptr;
using std::stringstream;

class AstNode : public std::enable_shared_from_this<AstNode> {
	public:
		AstNode() { reconstruction_state = -1; }
		AstNode(const AstNode*& node) { reconstruction_state = -1; }
		virtual ~AstNode() {}

		virtual string toJson() const {
			return "ASTNODE WUUUUUUUUUUUT";
		}

		virtual shared_ptr<Type> getType() const {
			std::cout << "ASTNODE TYPE WUUUUUUUUUUUT\n"; return nullptr;
		}

		virtual vector<shared_ptr<AstNode>> getChildrenForTypeCheck() const {
			return {};
		}

		bool reconstruction(int pass, std::map<string,string>& variables) {
			if ( reconstruction_state == pass ) return true;
			reconstruction_state = pass;

			return this->_reconstruction_action(pass, variables);
		}

		virtual llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
			throw std::logic_error("Function is not overloaded");
		}

	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables) {
			throw std::logic_error("Function is not overloaded");
		}
		int reconstruction_state = -1;
};
