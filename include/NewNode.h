#pragma once

#include <memory>
#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <iostream>
#include <boost/optional.hpp>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"

#include "Type.h"
#include "Types.h"
#include "AstNode.h"
#include "TypeNode.h"
#include "FunNode.h"

using std::string;
using std::vector;
using std::pair;
using boost::optional;
using std::weak_ptr;
using std::shared_ptr;
using std::stringstream;


class NewNode : public AstNode {
	public:
		NewNode(const shared_ptr<TypeNode>& ttype, const vector<shared_ptr<AstNode>>& pars);

		string toJson() const;

		shared_ptr<Type> getType() const;

		vector<shared_ptr<AstNode>> getChildrenForTypeCheck() const;
		
		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);

		static llvm::Function* getMalloc(llvm::LLVMContext& context, llvm::Module* module) {

			auto malloc = module->getFunction("bgc_malloc");
			if ( !malloc ) {
		
				vector<llvm::Type*> input = { llvm::IntegerType::get(context, 64) };
				auto funct = llvm::FunctionType::get(
					llvm::PointerType::getUnqual(llvm::IntegerType::get(context, 8)),
					input,
					false
				);
		
				malloc = llvm::Function::Create(
					funct,
					llvm::GlobalValue::ExternalLinkage,
					"bgc_malloc",
					module
				);
		
				malloc->setCallingConv(llvm::CallingConv::C);
			}

			return malloc;
		}

	private:
		shared_ptr<TypeNode> type;
		vector<shared_ptr<AstNode>> params;
		shared_ptr<FunNode> constructor;

	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables);
};
