#pragma once

#include <memory>
#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <iostream>
#include <boost/optional.hpp>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "Type.h"
#include "TypeNode.h"
#include "AstNode.h"
#include "DefineNode.h"
#include "BlockNode.h"

using std::string;
using std::vector;
using std::pair;
using boost::optional;
using std::shared_ptr;
using std::stringstream;

class FunNode : public AstNode {
	public:
		FunNode(const string& fname,
				const bool& is_entrypoint = false,
				const bool& is_constructor = false,
				const bool& is_class_function = false,
				const bool& is_native = false);

		FunNode(const FunNode& other);

		string toJson() const {
			stringstream sargs;
			for ( auto arg : args )
				sargs << arg->toJson() << ",";

			return "{'name': '" + name + "','rtype' : '" + rtype->name + "', 'args' : [" + sargs.str() + "], 'body': " + (native ? "'$native'" : body->toJson()) + "}";
		}

		shared_ptr<Type> getType() const;

		vector<shared_ptr<AstNode>> getChildrenForTypeCheck() const {
			vector<shared_ptr<AstNode>> children {args.begin(), args.end()};
			children.push_back(body);
			return children;
		}

		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);

		string getName();
		string getRawSignature();

		virtual llvm::Function* getLLVMSignature(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) const;

		bool canBeCalled(const vector<shared_ptr<AstNode>>& params);
		bool canBeCalled(const vector<shared_ptr<Type>>& arg_types);

		bool isProcedure() const {
			return rtype->is_defined && std::dynamic_pointer_cast<VoidType>(rtype->getType()) != nullptr;
		}

		Type* owner_class;
		shared_ptr<TypeNode> rtype;
		shared_ptr<BlockNode> body;
		vector<shared_ptr<DefineNode>> args;
		const bool entrypoint; // remove it when have return void and string array
		const bool constructor;
		const bool class_function;
		bool native;

	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables);

	private:
		string name;
		string signature;
};