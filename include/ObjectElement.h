#pragma once

#include <memory>
#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <iostream>
#include <boost/optional.hpp>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "FunNode.h"
#include "ObjectType.h"
#include "Types.h"


using std::string;
using std::vector;
using std::pair;
using boost::optional;
using std::shared_ptr;
using std::stringstream;

class ObjectElement : public Addressable {
	public:
		ObjectElement(const string& el, const shared_ptr<AstNode>& obj):
			Addressable(el),
			self(obj)
		{}

		string toJson() const {
			return "{'el' : '" + name + "', 'self': " + self->toJson() + "}";
		}

		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);

		static llvm::Value* getLLVMMethod(shared_ptr<FunNode> meth, llvm::Value* self, llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
			auto owner = (ObjectType*) meth->owner_class;
			auto vtable = builder.CreateLoad(builder.CreateStructGEP(
				builder.CreateBitCast(self, owner->getLLVMLiteralType(builder, context, module)),
				0
			));
			auto foundo = std::find(owner->methods.begin(), owner->methods.end(), meth);
			auto pos = std::distance(owner->methods.begin(), foundo);
			return builder.CreateLoad(builder.CreateStructGEP(vtable, pos));
		}

		shared_ptr<Type> getType() const {
			if ( auto obj_type = dynamic_cast<ArrayType*>(&*self->getType()) ) {
				return obj_type->elementType->getType();
			}
			return variable->getType();
		}

		vector<shared_ptr<AstNode>> getChildrenForTypeCheck() const {
			// @todo WTF?
			return {self};
		}

		llvm::Value* getAddr(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
			auto selfp = self->codegen(builder, context, module);
			auto self_type = self->getType();
			int pos = 0;
			// what to do with array?
			if ( auto obj_type = dynamic_cast<ObjectType*>(&*self_type) ) {
				auto foundo = std::find(obj_type->variables.begin(), obj_type->variables.end(), this->variable );
				pos = std::distance(obj_type->variables.begin(), foundo);

				return builder.CreateBitCast(
					builder.CreateStructGEP(
						builder.CreateBitCast(selfp, obj_type->getLLVMLiteralType(builder, context, module)),
					1 + pos),
					llvm::PointerType::getUnqual(variable->getType()->getLLVMType(builder, context, module)),
					"." + variable->name
				);
			}
			if ( dynamic_cast<ArrayType*>(&*self_type) ) {
				// our statistical forecasting system assumes it to be the variable believe "length" and nothing else,
				// hope it is stored on ZerooOOoo, good luck^_^
				return builder.CreateStructGEP(selfp, 0);
			}
			throw std::runtime_error("Wat type in " + this->toJson());
		}

		shared_ptr<AstNode> self;
		shared_ptr<VarNode> variable;

	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables);
};
