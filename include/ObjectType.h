#pragma once

#include <memory>
#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <iostream>
#include <iostream>
#include <boost/optional.hpp>
#include <functional>


#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "VarNode.h"
#include "FunNode.h"
#include "DefineNode.h"

using std::string;
using std::vector;
using std::pair;
using boost::optional;
using std::shared_ptr;
using std::stringstream;
using std::cout;
using std::endl;

class Namespace;

class ObjectType : public Type {
	public:
		ObjectType(const string& name, const vector<shared_ptr<VarNode>>& class_vars,
				   const vector<shared_ptr<FunNode>>& class_methods,
				   const optional<shared_ptr<TypeNode>>& class_super = optional<shared_ptr<TypeNode>>(),
				   const bool isExternal = false);

		string toJson() const;

		vector<shared_ptr<AstNode>> getChildrenForTypeCheck() const;

		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);

		llvm::Type* getLLVMType(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) const;
		llvm::Type* getLLVMLiteralType(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) const;

		virtual bool operator==(const Type& other) const {
			auto other_class = dynamic_cast<const ObjectType*>(&other);
			if (!other_class) {
				return false;
			}
			if (name == other_class->name) {
				if (!super && !other_class->super)
					return true;
				return super.get() == other_class->super.get();
			}
			return super && super.get()->getType() && *super.get()->getType() == other; // TODO: it's not eq! blame
		}

		// VERY BAD
		bool can_be_main() {
			for ( auto meth : methods )
				if (meth->getName() == "main") return true;
			return false;
		}

		template<class ArgType>
		shared_ptr<FunNode> findFunction(const vector<shared_ptr<ArgType>>& args,
				const std::function<bool(const shared_ptr<FunNode>& method)>& filter) {
			for ( auto meth : methods )
				if ( filter(meth) && meth->canBeCalled(args) )
					return meth;
			return nullptr;
		}


		shared_ptr<ObjectType> asExternal();
	public:
		vector<shared_ptr<VarNode>> variables;
		vector<shared_ptr<FunNode>> methods;
		shared_ptr<VarNode> self;
		const optional<shared_ptr<TypeNode>> super;
		const bool external;
		bool native = false;
		llvm::Type* nativeType = 0;

	protected:	
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables);

};
