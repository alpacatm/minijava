#pragma once

#include <memory>
#include <string>
#include <vector>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "Type.h"
#include "TypeNode.h"

using std::string;
using std::shared_ptr;
using std::vector;

class VoidType : public Type {
	public:
		VoidType(): Type(typeName) {}
		string toJson() const { return "{'type': '" + typeName + "'}"; }
		virtual llvm::Type* getLLVMType(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) const {
			return builder.getVoidTy();
		}

	public:
		static const string typeName;
};