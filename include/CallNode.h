#pragma once

#include <memory>
#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <iostream>
#include <sstream>
#include <algorithm>

#include <boost/optional.hpp>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "FunNode.h"

using std::string;
using std::vector;
using std::pair;
using boost::optional;
using std::shared_ptr;
using std::stringstream;

class CallNode : public AstNode {
	public:
		CallNode(const shared_ptr<AstNode>& obj, const string& func, const vector<shared_ptr<AstNode>>& arguments):
				self(obj),
				fname(func),
				params(arguments) {
		}

		string toJson() const {
			stringstream sparams;
			for ( auto param : params )
				sparams << param->toJson() << ",";
			return "{'self': " + self->toJson() + ", 'fname' : '" + fname + "', 'args' : [" + sparams.str() + "]}";
		}

		virtual shared_ptr<Type> getType() const;

		vector<shared_ptr<AstNode>> getChildrenForTypeCheck() const {
			vector<shared_ptr<AstNode>> children {params.begin(), params.end()};
			children.push_back(self);
			return children;
		}

		virtual string wrongArgumentsMessage() {
			return "Can't find function '" + fname + "' : "  + self->toJson();
		}

	llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);

	public:
		const shared_ptr<AstNode> self;
		shared_ptr<FunNode> function;

	protected:
		const string fname;
		const vector<shared_ptr<AstNode>> params;
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables);
};
