#pragma once

#include <memory>
#include <string>
#include <vector>
#include <set>
#include <utility>
#include <sstream>
#include <iostream>
#include <boost/optional.hpp>
#include <algorithm> 

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "Type.h"
#include "Types.h"
#include "AstNode.h"
#include "TypeNode.h"
#include "ObjectType.h"
#include "UsageNode.h"
#include "Namespace.h"

using std::string;
using std::vector;
using std::pair;
using boost::optional;
using std::shared_ptr;
using std::stringstream;


class Module : public AstNode {
	public:
		void addClass(const shared_ptr<ObjectType>& klass) { classes.push_back(klass); }
		void addUsage(const shared_ptr<UsageNode>& usage) { usages.push_back(usage); }

		string toJson() const;

		shared_ptr<Type> getType() const;

		vector<shared_ptr<AstNode>> getChildrenForTypeCheck() const;

		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);

		bool exportTypes(const shared_ptr<Namespace>& target_names);

		void fillUsages(std::set<string>& usages);

	public:
		bool check();
		bool compile(const string& outfile = "");

		string getPath(const bool& with_extension, const bool& external) const;

	public:
		const string name;
		const shared_ptr<Namespace> names;

	public:
		static shared_ptr<Module> findByName(const string& module_name);
		static shared_ptr<Module> fromSource(const string& source);

	private:
		Module(const string& module_name);

	private:
		shared_ptr<ObjectType> main;
		vector<shared_ptr<ObjectType>> classes;
		vector<shared_ptr<UsageNode>> usages;
		bool compiled = false;
		llvm::LLVMContext llvmContext;
		shared_ptr<llvm::Module> llvmModule = nullptr;

	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables);
};
