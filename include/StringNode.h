#pragma once

#include <string>
#include <memory>

#include "ValueNode.h"

using std::string;
using std::shared_ptr;

class StringNode : public ValueNode {
	public:
		StringNode(const string& val, const shared_ptr<TypeNode>& ttype) :
			ValueNode(ttype), value(val)
		{}

		string toJson() const {
			return "{'" + type->name + "' : '" + value + "'}";
		}

		shared_ptr<Type> getType() const {
			return type->getType();
		}

		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);

		const string value;
};