#pragma once

#include <memory>
#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <iostream>
#include <boost/optional.hpp>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "Type.h"
#include "ControlFlow.h"
#include "AstNode.h"
#include "TypeNode.h"
#include "ObjectType.h"

using std::string;
using std::vector;
using std::pair;
using std::shared_ptr;
using std::stringstream;
using boost::optional;

class ReturnNode : public ControlFlow  {
	public:
		ReturnNode(const optional<shared_ptr<AstNode>> ret_expr):ret_expr(ret_expr) {}

		string toJson() const {
			return "{'return' : " + ( ret_expr ? ret_expr.get()->toJson() : "'undefined'") + "}";
		}

		shared_ptr<Type> getType() const {
			return ret_expr ? ret_expr.get()->getType() : shared_ptr<Type>(new VoidType());
		}

		vector<shared_ptr<AstNode>> getChildrenForTypeCheck() const {
			return {}; // Because we check rtype type in ReturnNode::getType
		}

		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);

	public:
		virtual bool isTerminator() { return true; }
		virtual list<shared_ptr<ReturnNode>> getReturns() {
			return { std::dynamic_pointer_cast<ReturnNode>(shared_from_this()) };
		}
	public:
		const optional<shared_ptr<AstNode>> ret_expr;

	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables);
};
