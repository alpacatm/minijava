#pragma once

#include <memory>
#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <iostream>
#include <boost/optional.hpp>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/ValueSymbolTable.h"

#include "Type.h"
#include "TypeNode.h"
#include "Addressable.h"

class VarNode : public Addressable {
	public:
		VarNode(const string& id, const shared_ptr<TypeNode>& typ):
			Addressable(id), type(typ), is_class(false)
		{}

		string toJson() const {
			return "{'var' : '" + name + "', 'isa': '" + type->name + "'}";
		}

		shared_ptr<Type> getType() const {
			return type->getType();
		}

		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);

	public:
		shared_ptr<TypeNode> type;
		bool is_class;

	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables);
};
