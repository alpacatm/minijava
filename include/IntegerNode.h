#pragma once

#include <string>
#include <memory>

#include "ValueNode.h"

using std::string;
using std::shared_ptr;

class IntegerNode : public ValueNode {
	public:
		IntegerNode(const IntegerNode*& node) :
			IntegerNode(node->value, node->type)
		{}

		IntegerNode(const int& val, const shared_ptr<TypeNode>& ttype):
			ValueNode(ttype), value(val)
		{}

		string toJson() const {
			stringstream ss;
			ss << "{'" << type->name << "' : " << value << "}";
			return ss.str();
		}

		shared_ptr<Type> getType() const {
			return type->getType();
		}

		const int value;
		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);
	
	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables) {
			return true;
		}

};