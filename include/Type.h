#pragma once

#include <string>
#include <iostream>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

using std::string;
using std::shared_ptr;

class Type {
	public:
		const string name;
	public:
		Type(const string& name): name(name) { reconstruction_state = -1; }
		virtual ~Type() {}

		virtual string toJson() const {
			return "TYPE WUUUUUUUUUUUT";
		}

		bool reconstruction(int pass, std::map<string,string>& variables) {
			if ( reconstruction_state == pass ) return true;
			reconstruction_state = pass;
			auto res = this->_reconstruction_action(pass, variables);
			return res;
		}

		virtual llvm::Type* getLLVMType(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) const { return 0; }

		virtual bool operator==(const Type& other) const {
			return name == other.name;
		}

		virtual bool operator!=(const Type& other) const {
			return !operator==(other);
		}

	protected:
		int reconstruction_state = -1;

		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables) {
			return true;
		}
};
