#pragma once

#include <string>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "AstNode.h"

using std::string;

class Addressable : public AstNode {
	public:
		void setAddr(llvm::Value* add, bool force = false) { if ( not addr || force ) addr = add; }
		virtual llvm::Value* getAddr(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
			return addr;
		}

	protected:
		Addressable(const string& id): name(id) {}

	public:
		const string name;

	protected:
		llvm::Value * addr = 0;
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables) {
			return true;
		}
};
