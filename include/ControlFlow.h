#pragma once
#include <list>
#include "AstNode.h"

using std::list;

class ReturnNode;
class ControlFlow : public AstNode {
	public:
		virtual bool isTerminator() { return false; }
		virtual list<shared_ptr<ReturnNode>> getReturns() { return {}; }
	protected:
		static bool isPrevBlocTerminator(llvm::IRBuilder<>& builder) {
			auto currInsertPoint = builder.GetInsertPoint();
			auto currInsertBlock = builder.GetInsertBlock();
			if ( currInsertBlock->getFirstInsertionPt() != currInsertPoint )
				return currInsertPoint->getPrevNode()->isTerminator();
			return false;
		}
};