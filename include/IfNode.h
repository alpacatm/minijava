#pragma once

#include <memory>
#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <iostream>
#include <boost/optional.hpp>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "Type.h"
#include "VoidType.h"
#include "TypeNode.h"
#include "Addressable.h"
#include "ControlFlow.h"


using std::string;
using std::vector;
using std::pair;
using boost::optional;
using std::shared_ptr;
using std::stringstream;

class IfNode : public ControlFlow {
	public:
		IfNode(const shared_ptr<AstNode>& cond, const shared_ptr<AstNode>& act, const optional<shared_ptr<AstNode>>& other):condition(cond),action(act),otherwise(other) {}

		string toJson() const {
			return "{'if' : " + condition->toJson() + ", 'then':" + action->toJson() + (otherwise ? ", 'otherwise':" + otherwise.get()->toJson() : "") + "}";
		}

		shared_ptr<Type> getType() const {
			return shared_ptr<Type>(new VoidType());
		}

		vector<shared_ptr<AstNode>> getChildrenForTypeCheck() const {
			vector<shared_ptr<AstNode>> children {condition, action};
			if (otherwise) children.push_back(otherwise.get());
			return children;
		}

		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);

	public:
		virtual bool isTerminator();
		virtual list<shared_ptr<ReturnNode>> getReturns();

	public:
		const shared_ptr<AstNode> condition;
		const shared_ptr<AstNode> action;
		const optional<shared_ptr<AstNode>> otherwise;

	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables);
};
