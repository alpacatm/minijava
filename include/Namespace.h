#pragma once

#include <list>
#include <vector>
#include <map>
#include <string>
#include <memory>
#include <exception>
#include <boost/optional.hpp>

#include "Types.h"
#include "VarNode.h"
#include "ObjectType.h"

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Module.h"

using std::list;
using std::map;
using std::string;
using std::shared_ptr;
using std::runtime_error;
using boost::optional;

class Scope {
	public:
		Scope(bool ordered) : is_ordered(ordered) {}

	public:
		bool is_ordered;
		map<string, shared_ptr<VarNode>> names = {};
		map<string, shared_ptr<TypeNode>> types = {};
};

static const shared_ptr<TypeNode> JUNK_TYPE {new TypeNode("$junk")};

class Namespace {
	public:
		Namespace();
		~Namespace() {
			scopes.clear();
		}

		shared_ptr<VarNode> getVar(const string& name);
		shared_ptr<VarNode> addVar(const string& name, const shared_ptr<TypeNode> type);

		shared_ptr<TypeNode> getType(const string& name);
		shared_ptr<TypeNode> addType(shared_ptr<Type> name);

		void pushScope(bool is_ordered = false);
		vector<shared_ptr<VarNode>> popScope();

		bool importType(const shared_ptr<ObjectType>& type);
		bool exportTypes(const shared_ptr<Namespace>& names);

		void classesAsVariables();

		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);

	public:
		shared_ptr<TypeNode> getVoidType();
		shared_ptr<TypeNode> getAutoType();
		shared_ptr<TypeNode> getIntegerType();
		shared_ptr<TypeNode> getBooleanType();
		shared_ptr<TypeNode> getUStringType();
		shared_ptr<TypeNode> getStringType();

	protected:
		optional<shared_ptr<VarNode>> findVar(const string& name);
		optional<shared_ptr<TypeNode>> findType(const string& name);

		shared_ptr<Scope> currentScope(bool unordered = false);

	private:
		list<shared_ptr<Scope>> scopes = {};

		vector<shared_ptr<AstNode>> trash = {};
};