#pragma once

#include "CallNode.h"

class ExprNode : public CallNode {
	public:
		ExprNode(const shared_ptr<AstNode>& obj, const string& func, const vector<shared_ptr<AstNode>>& arguments):CallNode(obj, func, arguments) {
		}

		string wrongArgumentsMessage();
};
