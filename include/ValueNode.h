#pragma once

#include <memory>
#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <iostream>
#include <boost/optional.hpp>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "Type.h"
#include "Types.h"
#include "AstNode.h"
#include "TypeNode.h"
#include "Addressable.h"

using std::string;
using std::vector;
using std::pair;
using boost::optional;
using std::shared_ptr;
using std::stringstream;


class ValueNode : public AstNode {
	public:
		ValueNode(shared_ptr<TypeNode> ttype) : type(ttype) {}

		const shared_ptr<TypeNode> type;

	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables) {
			return true;
		}
};