#pragma once

#include <memory>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <boost/optional.hpp>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "Type.h"
#include "Types.h"
#include "AstNode.h"
#include "TypeNode.h"
#include "Addressable.h"

using std::string;
using std::vector;
using std::shared_ptr;
using std::stringstream;

class ArrayElement : public Addressable {
	public:
		ArrayElement(const shared_ptr<AstNode>& elem, const shared_ptr<AstNode>& arr);

		string toJson() const;

		shared_ptr<Type> getType() const;

		vector<shared_ptr<AstNode>> getChildrenForTypeCheck() const;

		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);

		llvm::Value* getAddr(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);
	public:
		shared_ptr<AstNode> el;
		shared_ptr<AstNode> array;
	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables);
};
