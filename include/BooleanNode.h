#pragma once

#include <string>
#include <memory>

#include "ValueNode.h"

using std::string;
using std::shared_ptr;

class BooleanNode : public ValueNode {
	public:
		BooleanNode(const BooleanNode*& node):
			BooleanNode(node->value, node->type)
		{}

		BooleanNode(const bool& val, const shared_ptr<TypeNode>& ttype):
			ValueNode(ttype), value(val)
		{}

		string toJson() const {
			stringstream ss;
			ss << "{'" << type->name << "' : '" << (value ? "true" : "false") << "'}";
			return ss.str();
		}

		shared_ptr<Type> getType() const {
			return type->getType();
		}

		const bool value;
		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);
};
