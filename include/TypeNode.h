#pragma once

#include <memory>
#include <string>

#include "Type.h"
#include "AstNode.h"

using std::string;
using std::shared_ptr;

class TypeNode : public AstNode {
	public:
		TypeNode(const string& tname, bool defined = false) : is_defined(defined), name(tname) { }

		TypeNode(shared_ptr<Type> type) : is_defined(true), name(type->name), type(type) { }

		string toJson() const {
			if (type)
				return type->toJson();

			return "{'type' : '" + name + "', 'undefined': 1}";
		}

		shared_ptr<Type> getType() const {
			return type;
		}

		void setType(shared_ptr<Type> type) {
			if (is_defined)
				throw std::logic_error("'" + name + "' type already has definition");

			is_defined = true;
			this->type = type;
		}

	public:
		bool is_defined;
		const string name;

	private:
		shared_ptr<Type> type;

	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables);
};
