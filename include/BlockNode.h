#pragma once

#include <memory>
#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <iostream>
#include <boost/optional.hpp>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "Type.h"
#include "VoidType.h"
#include "TypeNode.h"
#include "ControlFlow.h"

using std::string;
using std::vector;
using std::pair;
using boost::optional;
using std::weak_ptr;
using std::shared_ptr;
using std::stringstream;

class BlockNode : public ControlFlow {
	public:
		BlockNode();
		BlockNode(const vector<shared_ptr<AstNode>>& exprs);

		string toJson() const;

		shared_ptr<Type> getType() const;

		vector<shared_ptr<AstNode>> getChildrenForTypeCheck() const;

		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);

	public:
		virtual bool isTerminator();
		virtual list<shared_ptr<ReturnNode>> getReturns();

	public:
		const vector<shared_ptr<AstNode>> body;

	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables);
};
