#pragma once

#include <memory>
#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <iostream>
#include <boost/optional.hpp>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "VarNode.h"

class DefineNode : public VarNode {
	public:
		DefineNode(const shared_ptr<VarNode> variable):
			VarNode(*variable),
			var(variable)
		{}

		string toJson() const {
			return "{'define' : " + var->toJson() + "}";
		}

		shared_ptr<Type> getType() const {
			return var->getType();
		}

		vector<shared_ptr<AstNode>> getChildrenForTypeCheck() const {
			return {var};
		}
		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);
		llvm::Value* getAddr(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) {
			if ( not var->getAddr(builder, context, module) )
				codegen(builder, context, module);
			return var->getAddr(builder, context, module);
		}

	public:
		const shared_ptr<VarNode> var;

	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables);
};
