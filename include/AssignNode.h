#pragma once

#include <memory>
#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <iostream>
#include <boost/optional.hpp>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "Type.h"
#include "Types.h"
#include "AstNode.h"
#include "TypeNode.h"
#include "Addressable.h"

using std::string;
using std::vector;
using std::pair;
using boost::optional;
using std::shared_ptr;
using std::stringstream;

class AssignNode : public AstNode {
	public:
		AssignNode(const shared_ptr<Addressable> lvalue, const shared_ptr<AstNode> rvalue);
		string toJson() const;

		shared_ptr<Type> getType() const;

		vector<shared_ptr<AstNode>> getChildrenForTypeCheck() const;

		const shared_ptr<Addressable> var;
		const shared_ptr<AstNode> expr;

		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);

	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables);
};
