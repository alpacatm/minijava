#pragma once

#include <memory>
#include <string>
#include <vector>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "Type.h"
#include "TypeNode.h"

using std::string;
using std::shared_ptr;
using std::vector;

class FunType : public Type {
	public:
		FunType(const bool& varargs): Type("somefun"), varArg(varargs) {}
		const shared_ptr<TypeNode> result;
		const bool varArg;
		const vector<shared_ptr<TypeNode>> args;
};

// TODO remove this so much kostyl
class ArrayType : public Type {
	public:
		ArrayType(const shared_ptr<TypeNode> subtype):
				Type(subtype->name + "[]"),
				elementType(subtype)
		{}

		string toJson() const { return "{'type': 'array', 'of': " + elementType->toJson() + "}"; }

		virtual llvm::Type* getLLVMType(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) const;

		shared_ptr<TypeNode> elementType;

	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables) {
			return elementType->reconstruction(pass, variables);
		}
};
