#pragma once

#include <memory>
#include <string>
#include <vector>
#include <set>
#include <utility>
#include <sstream>
#include <iostream>
#include <boost/optional.hpp>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "Type.h"
#include "VoidType.h"
#include "AstNode.h"
#include "TypeNode.h"
#include "Addressable.h"
#include "Namespace.h"

using std::string;
using std::vector;
using std::pair;
using boost::optional;
using std::shared_ptr;
using std::stringstream;

class UsageNode : public AstNode {
	public:
		UsageNode(const string& mod, const shared_ptr<Namespace>& names):module(mod),target_namespace(names) {}

		string toJson() const {
			return "{'use' : '" + module + "'}";
		}

		shared_ptr<Type> getType() const {
			return shared_ptr<Type>(new VoidType());
		}

		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module) { return 0; }

		void fillUsages(std::set<string>& usages);
	private:
		const string module;
		const shared_ptr<Namespace> target_namespace;
		static std::map<string, shared_ptr<AstNode>> cache;

	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables);
};
