#pragma once

#include <string>
#include <iostream>
#include <map>

#include "AstNode.h"

using std::string;
using std::weak_ptr;
using std::shared_ptr;

enum PassPhase {
	FIRST_PASS,
	SECOND_PASS,
	THIRD_PASS,
	FOURTH_PASS,
	FIFTH_PASS,
	POST_LAST
};


class BlockNode;
class SemanticAnalyzer {
	private:
		shared_ptr<AstNode> node;

	public:
		SemanticAnalyzer(shared_ptr<AstNode> node): node(node)
		{}

		bool check() {
			return checkNode(node);
		}


		bool checkNode(shared_ptr<AstNode> node);

		bool reconstruction() {
			for ( int pass = FIRST_PASS; pass <= POST_LAST; pass++ )
				if ( !node->reconstruction(pass, variables) )
					return false;
			return true;
		}

		std::map<string,string> variables;
};