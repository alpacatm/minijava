#pragma once

#include <memory>
#include <string>
#include <vector>
#include <utility>
#include <sstream>
#include <iostream>
#include <boost/optional.hpp>

#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/Module.h"

#include "Type.h"
#include "VoidType.h"
#include "TypeNode.h"
#include "ObjectType.h"
#include "ControlFlow.h"

using std::string;
using std::vector;
using std::shared_ptr;
using std::stringstream;

class WhileNode : public ControlFlow {
	public:
		WhileNode(const shared_ptr<AstNode>& exp, const shared_ptr<AstNode>& st);

		string toJson() const;

		shared_ptr<Type> getType() const;

		vector<shared_ptr<AstNode>> getChildrenForTypeCheck() const;

		llvm::Value* codegen(llvm::IRBuilder<>& builder, llvm::LLVMContext& context, llvm::Module* module);

	public:
		virtual bool isTerminator() { return false; }
		virtual list<shared_ptr<ReturnNode>> getReturns();

	private:
		const shared_ptr<AstNode> expression;
		const shared_ptr<AstNode> statement;

	protected:
		virtual bool _reconstruction_action(int pass, std::map<string,string>& variables);
};
