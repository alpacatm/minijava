#!/usr/bin/perl

use strict;
use warnings;
use 5.010;

use Cwd 'abs_path';

(my $abs = abs_path($0)) =~ s@/[^/]+$@@;

my $src = shift;
my $ll = "$src.ll";
my $mcomp = "$src.s";
my $final = "$src.out";
system("cp $src $ll");
system("llc $ll -o $mcomp");
system("clang $mcomp $abs/src/bgc.ll -lgc -o $final");
say $final;